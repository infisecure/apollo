package com.apollo.constants;

public enum ServiceCodeEnum {

	WEB_AND_WEBAPI("WWAPI"), REST_AND_MOBILEAPI("RMAPI"), BRUTEFORCE_PROTECTION("BFPTN");

	private String serviceCode;

	private ServiceCodeEnum(final String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	@Override
	public String toString() {
		return serviceCode;
	}

	public String getName() {
		return this.name();
	}

}
