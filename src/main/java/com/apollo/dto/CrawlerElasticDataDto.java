package com.apollo.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class CrawlerElasticDataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private CrawlerElasticDataDto() {
		super();
	}

	private String iscr1;
	private String iscr2;
	private String iscr3;
	private String iscr4;
	private String iscr5;
	private String iscr6;
	private Date iscr7;
	private int iscr8;
	private int iscr9;
	private int iscr10;
	private String iscr11;
	private String iscr12;
	private String iscr13;
	private String iscr14;
	private String iscr15;
	private String iscr16;
	private String iscr17;
	private String iscr18;
	private String iscr19;
	private String iscr20;
	private String iscr21;
	private long iscr22;
	private String iscr23;

	private CrawlerElasticDataDto(CrawlerElasticDataBuilder crawlerElasticDataBuilder) {
		super();
		this.iscr1 = crawlerElasticDataBuilder.iscr1;
		this.iscr2 = crawlerElasticDataBuilder.iscr2;
		this.iscr3 = crawlerElasticDataBuilder.iscr3;
		this.iscr4 = crawlerElasticDataBuilder.iscr4;
		this.iscr5 = crawlerElasticDataBuilder.iscr5;
		this.iscr6 = crawlerElasticDataBuilder.iscr6;
		this.iscr7 = crawlerElasticDataBuilder.iscr7;
		this.iscr8 = crawlerElasticDataBuilder.iscr8;
		this.iscr9 = crawlerElasticDataBuilder.iscr9;
		this.iscr10 = crawlerElasticDataBuilder.iscr10;
		this.iscr11 = crawlerElasticDataBuilder.iscr11;
		this.iscr12 = crawlerElasticDataBuilder.iscr12;
		this.iscr13 = crawlerElasticDataBuilder.iscr13;
		this.iscr14 = crawlerElasticDataBuilder.iscr14;
		this.iscr15 = crawlerElasticDataBuilder.iscr15;
		this.iscr16 = crawlerElasticDataBuilder.iscr16;
		this.iscr17 = crawlerElasticDataBuilder.iscr17;
		this.iscr18 = crawlerElasticDataBuilder.iscr18;
		this.iscr19 = crawlerElasticDataBuilder.iscr19;
		this.iscr20 = crawlerElasticDataBuilder.iscr20;
		this.iscr21 = crawlerElasticDataBuilder.iscr21;
		this.iscr22 = crawlerElasticDataBuilder.iscr22;
		this.iscr23 = crawlerElasticDataBuilder.iscr23;
	}

	public static class CrawlerElasticDataBuilder {
		private String iscr1;
		private String iscr2;
		private String iscr3;
		private String iscr4;
		private String iscr5;
		private String iscr6;
		private Date iscr7;
		private int iscr8;
		private int iscr9;
		private int iscr10;
		private String iscr11;
		private String iscr12;
		private String iscr13;
		private String iscr14;
		private String iscr15;
		private String iscr16;
		private String iscr17;
		private String iscr18;
		private String iscr19;
		private String iscr20;
		private String iscr21;
		private long iscr22;
		private String iscr23;

		public CrawlerElasticDataBuilder(final ServerRequestDataDto serverRequestDataDto) {
			this.iscr1 = serverRequestDataDto.getIsar32();
			this.iscr2 = serverRequestDataDto.getIsar1();
			this.iscr3 = serverRequestDataDto.getIsar3();
			this.iscr4 = serverRequestDataDto.getIsar4();
			this.iscr5 = serverRequestDataDto.getIsar5();
			this.iscr6 = serverRequestDataDto.getIsar6();
			this.iscr7 = new Date(Long.parseLong(serverRequestDataDto.getIsar8()));
			this.iscr11 = serverRequestDataDto.getIsar9();
			this.iscr12 = serverRequestDataDto.getIsar20();
			this.iscr13 = serverRequestDataDto.getIsar10();
			this.iscr18 = serverRequestDataDto.getIsar19();
			this.iscr19 = serverRequestDataDto.getIsar29();
			this.iscr20 = serverRequestDataDto.getIsar33();
			this.iscr21 = serverRequestDataDto.getIsar34();
			this.iscr23 = serverRequestDataDto.getIsar36();
		}

		public CrawlerElasticDataBuilder minute(final int minute) {
			this.iscr8 = minute;
			return this;
		}

		public CrawlerElasticDataBuilder hour(final int hour) {
			this.iscr9 = hour;
			return this;
		}

		public CrawlerElasticDataBuilder dayOfWeek(final int dayOfWeek) {
			this.iscr10 = dayOfWeek;
			return this;
		}

		public CrawlerElasticDataBuilder crawlerDetails(final String crawlerName, final String crawlerFamily,
				final String crawlerCategory, final String crawlerIdentificationCode) {
			this.iscr14 = crawlerName;
			this.iscr15 = crawlerFamily;
			this.iscr16 = crawlerCategory;
			this.iscr17 = crawlerIdentificationCode;
			return this;
		}

		public CrawlerElasticDataBuilder processingTimeDifference(final long timeDifference) {
			this.iscr22 = timeDifference;
			return this;
		}

		public CrawlerElasticDataDto build() {
			return new CrawlerElasticDataDto(this);
		}
	}
}
