package com.apollo.constants;

public enum HttpRequestMethodEnum {

	GET("GET"), HEAD("HEAD"), POST("POST"), PUT("PUT"), DELETE("DELETE"), CONNECT("CONNECT"), OPTIONS("OPTIONS"), TRACE(
			"TRACE"), PATCH("PATCH");

	private String requestMethod;

	private HttpRequestMethodEnum(final String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	@Override
	public String toString() {
		return requestMethod;
	}

	public String getName() {
		return this.name();
	}

}
