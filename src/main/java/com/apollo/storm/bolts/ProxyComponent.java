package com.apollo.storm.bolts;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.BotBehaviourEnum;
import com.apollo.constants.BotCategoryEnum;
import com.apollo.constants.BotClassificationEnum;
import com.apollo.constants.Constant;
import com.apollo.constants.KafkaTopicEnum;
import com.apollo.constants.RequestCategoryEnum;
import com.apollo.constants.RequestDataEnum;
import com.apollo.constants.ServiceCodeEnum;
import com.apollo.dto.BadBotInformationDto.BadBotInformationBuilder;
import com.apollo.dto.DeviceInformationDto;
import com.apollo.dto.HumanElasticDataDto;
import com.apollo.dto.HumanElasticDataDto.HumanElasticDataBuilder;
import com.apollo.dto.ServerRequestDataDto;
import com.apollo.dto.UriDetailsDto;
import com.apollo.kafka.ApolloProducer;
import com.apollo.storm.jedis.JedisConnectionPool;
import com.apollo.util.AclUtil;
import com.apollo.util.CacheHandlerUtil;
import com.apollo.util.DateUtil;
import com.apollo.util.StringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;

public class ProxyComponent extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ProxyComponent.class);

	private ObjectMapper mapper;
	private transient OutputCollector outputCollector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map map, TopologyContext context, OutputCollector collector) {
		LOGGER.info("Preparing Proxy IP Address Component Task Bolt.");
		int totalTasks = context.getComponentTasks(RequestCategoryEnum.PROXY_IP.getRequestCategory()).size();
		LOGGER.debug("Total Task, {}", totalTasks);
		this.outputCollector = collector;
		this.mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
	}

	@Override
	public void execute(Tuple tuple) {
		/** get input data from the tuple */
		String inputData = tuple.getString(0);
		Jedis jedis = null;

		/** check if the input data is not null or empty string */
		if (StringUtil.isNotEmpty(inputData)) {
			LOGGER.debug("Processing Proxy IP API data, {}", inputData);
			ServerRequestDataDto requestDataDto = null;
			try {
				/** convert json string to request object */
				requestDataDto = mapper.readValue(inputData, ServerRequestDataDto.class);

				/** get required attributes from request json */
				String domainCode = requestDataDto.getIsar1();
				String refererUrl = StringUtil.getNotNullValue(requestDataDto.getIsar3()).trim();
				String ipAddress = StringUtil.getNotNullValue(requestDataDto.getIsar9()).trim();

				/** get path and page name from the requested url */
				String requestedUrl = StringUtil.getNotNullValue(requestDataDto.getIsar4()).trim();
				UriDetailsDto uriDetailsDto = CacheHandlerUtil.processUrl(requestedUrl);

				/** get device details details from the user-agent */
				String userAgent = requestDataDto.getIsar10();
				DeviceInformationDto deviceInfoDto = CacheHandlerUtil.getUseragentDetails(userAgent);

				/**
				 * convert the time stamp to date and get minute, hour and day of week
				 */
				long requestTime = Long.parseLong(requestDataDto.getIsar8());
				Date apiHitTime = new Date(requestTime);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(apiHitTime);
				int minute = calendar.get(Calendar.MINUTE);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

				String pageSequenceCookie = StringUtil.getNotNullValue(requestDataDto.getIsar14().trim());
				String[] pageSequenceCookieArr = pageSequenceCookie.split("\\.");
				int incrementedValue = StringUtil.getIncrementedValue(pageSequenceCookieArr[0]);
				int pageNumber = StringUtil.isNotEmpty(pageSequenceCookieArr[2])
						? Integer.parseInt(pageSequenceCookieArr[2])
						: 0;

				Date firstSeen = null;
				int daysDifference = 0;
				long firstSeenTimestamp = 0;
				String[] identifierCookie = requestDataDto.getIsar12().trim().split("\\.");
				if (identifierCookie.length == 3) {
					firstSeenTimestamp = Long.parseLong(identifierCookie[identifierCookie.length - 1]);
					firstSeen = new Date(firstSeenTimestamp);
					daysDifference = DateUtil.getDaysDiff(apiHitTime, firstSeen);
				}

				long sessionDuration = DateUtil.getSessionDuration(requestTime, pageSequenceCookieArr[1]);
				long processingTime = Long.parseLong(requestDataDto.getIsar35()) - requestTime;

				/**
				 * update the counters and their respective expiry time in redis
				 */
				StringBuilder ipRequestDetailsKey = new StringBuilder();
				ipRequestDetailsKey.append(Constant.REQUEST_DETAILS_HASH_KEY).append(Constant.COLON_DELIMITER)
						.append(domainCode).append(Constant.COLON_DELIMITER).append(ipAddress);
				StringBuilder reqPerMinKey = new StringBuilder(Constant.REQUEST_PER_MIN_HASH_KEY)
						.append(Constant.COLON_DELIMITER).append(domainCode).append(Constant.COLON_DELIMITER)
						.append(ipAddress);

				/** get jedis connection */
				jedis = JedisConnectionPool.getJedisConnection();

				if (isNonStandardUa(deviceInfoDto))
					jedis.hset(Constant.NON_STANDARD_UA_HASH_KEY, userAgent, "1");

//				long totalRequest = jedis.hincrBy(ipRequestDetailsKey.toString(), Constant.TOTAL_REQUEST, 1);

				long totalNonAjaxRequest = 0;
				if (!Constant.AJAX_REQUEST_IDENTIFIER.equalsIgnoreCase(requestDataDto.getIsar22())) {
					totalNonAjaxRequest = jedis.hincrBy(ipRequestDetailsKey.toString(), Constant.TOTAL_NON_AJAX_REQUEST,
							1);
					jedis.rpush(reqPerMinKey.toString(), String.valueOf(requestTime));
					jedis.expire(reqPerMinKey.toString(), 60);
				}

				long totalRequestSameRefReq = 0;
				if (StringUtil.isNotEmpty(refererUrl) && refererUrl.endsWith(requestedUrl))
					totalRequestSameRefReq = jedis.hincrBy(ipRequestDetailsKey.toString(),
							Constant.SAME_REQUEST_REFERER, 1);
				jedis.expire(ipRequestDetailsKey.toString(), 1800);

				HumanElasticDataDto humanElasticDataDto = new HumanElasticDataBuilder(requestDataDto)
						.pageDetails(uriDetailsDto).deviceDetails(deviceInfoDto).minute(minute).hour(hour)
						.dayOfWeek(dayOfWeek).numDaysActive(daysDifference).sessionDuration(sessionDuration)
						.pageSequence(pageNumber).processingTimeDifference(processingTime).build();

				StringBuilder aclRateLimiting = new StringBuilder();
				aclRateLimiting.append(Constant.ACL_CONFIG_RATE_LIMITING_HASH_KEY).append(Constant.COLON_DELIMITER)
						.append(domainCode).append(Constant.COLON_DELIMITER)
						.append(ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode());
				Map<String, String> aclRateLimitingMap = jedis.hgetAll(aclRateLimiting.toString());
				boolean isRateLimited = false;
				if (totalNonAjaxRequest > Integer.parseInt(aclRateLimitingMap.get("req_per_minute"))) {
					long startTime = Long.parseLong(jedis.lpop(reqPerMinKey.toString()));
					long timeInSec = requestTime - startTime;
					if (timeInSec < (60000)) {
						isRateLimited = true;
						rateLimitPagePerMin(domainCode, ipAddress);
					}
				}

				if (!isRateLimited
						&& (sessionDuration > Integer.parseInt(aclRateLimitingMap.get("session_duration")))) {
					isRateLimited = true;
					rateLimitSessionDuration(domainCode, ipAddress, humanElasticDataDto.getIsar23());
				}

				if (!isRateLimited)
					performStaticAnalysis(aclRateLimitingMap, humanElasticDataDto, deviceInfoDto, totalNonAjaxRequest,
							totalRequestSameRefReq, pageNumber, incrementedValue);

				ApolloProducer.publishMessage(KafkaTopicEnum.HUMAN.getKafkaTopic(),
						mapper.writeValueAsString(humanElasticDataDto));

				outputCollector.ack(tuple);
			} catch (IOException | ArrayIndexOutOfBoundsException e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception while parsing input data, {}", e);
			} catch (Exception e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception, {}", e);
			} finally {
				JedisConnectionPool.close(jedis);
			}
		} else {
			outputCollector.ack(tuple);
		}
	}

	private void performStaticAnalysis(Map<String, String> aclRateLimitingMap, HumanElasticDataDto humanElasticDataDto,
			DeviceInformationDto deviceInfoDto, long totalRequest, long totalRequestSameRefReq, int pageNumber,
			int incrementedValue) throws JsonProcessingException {
		String domainCode = StringUtil.getNotNullValue(humanElasticDataDto.getIsar1());
		String ipAddress = StringUtil.getNotNullValue(humanElasticDataDto.getIsar15()).trim();

		/** perform static analysis such as rate limiting, header validation */
		String validationOutcome = AclUtil.analyzeHumanRequest(aclRateLimitingMap, humanElasticDataDto, deviceInfoDto,
				totalRequest, totalRequestSameRefReq, pageNumber, incrementedValue);

		switch (validationOutcome) {
		case "BLACKLIST-BAD-REQUEST-METHOD":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-1").paramKey(RequestDataEnum.REQUEST_METHOD.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar6().trim())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass("Hacker - Trace Request Method")
							.botBehaviour(BotBehaviourEnum.VULNERABILITY_SCANNING.getBotBehaviour())
							.message(
									"Request made using HTTP TRACE, Attackers can run a cross-site-scripting attack on your server.")
							.build()));
			break;

		case "BLACKLIST-SAME-REQ-REF":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-2").paramKey(RequestDataEnum.IP_ADDRESS.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar9())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass("Improper Referer URL")
							.botBehaviour(BotBehaviourEnum.WEBSITE_EXTRACTOR.getBotBehaviour())
							.message("Attackers spoofing the Referer URL to match Request URL to avoid detection.")
							.build()));
			break;

		case "BLACKLIST-EMPTY-REFERER":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-3").paramKey(RequestDataEnum.REFERER_URL.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar3())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass("Empty Referer URL").botBehaviour(BotBehaviourEnum.SCRAPER_BOT.getBotBehaviour())
							.message(
									"Attackers running programmatic scripts to access the content and browse through the website.")
							.build()));
			break;

		case "BLACKLIST-SQL-INJECTION":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-4").paramKey(RequestDataEnum.IP_ADDRESS.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar9())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass("SQL/Script Injection").botBehaviour(BotBehaviourEnum.HACKER.getBotBehaviour())
							.message("Attackers performing Header based SQL/Script injection attack.").build()));
			break;

		case "TAMPERED-COOKIE":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-5").paramKey(RequestDataEnum.IP_ADDRESS.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar9())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass("SQL/Script Injection").botBehaviour(BotBehaviourEnum.HACKER.getBotBehaviour())
							.message("Attackers performing Header based SQL/Script injection attack.").build()));
			break;

		case "NON-STANDARD-UA":
			ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.ruleName("ISR-STRMG-6").paramKey(RequestDataEnum.USER_AGENT.getAttributeCode())
							.paramValue(humanElasticDataDto.getIsar16())
							.botCategory(BotCategoryEnum.INFISECURE_INTEGRITY.getBotCategory())
							.botClass(BotClassificationEnum.NON_STANDARD_UA.getBotClassification())
							.botBehaviour(BotBehaviourEnum.WEBSITE_EXTRACTOR.getBotBehaviour())
							.message("Attackers spoofing the Referer URL to match Request URL to avoid detection.")
							.build()));
			break;

		case "ANALYZE-IP-ADDRESS":
			ApolloProducer.publishMessage(KafkaTopicEnum.ANALYZE_REQUEST.getKafkaTopic(), mapper.writeValueAsString(
					new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
							.build()));
			break;

		default:
			break;
		}
	}

	private boolean isNonStandardUa(DeviceInformationDto deviceInfoDto) {
		return ("Other".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Library".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Offline Browser".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Robot".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "UNKNOWN".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Useragent Anonymizer".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| StringUtil.isEmpty(deviceInfoDto.getUserAgentType()));
	}

	private void rateLimitPagePerMin(String domainCode, String ipAddress) throws JsonProcessingException {
		ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
				new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
						.ruleName("ISR-STRMG-7").paramKey(RequestDataEnum.IP_ADDRESS.getAttributeCode())
						.paramValue(ipAddress).botCategory(BotCategoryEnum.RATE_LIMITING.getBotCategory())
						.botClass(BotClassificationEnum.PAGE_PER_MINUTE_EXCEEDED.getBotClassification())
						.botBehaviour(BotBehaviourEnum.UNCATEGORIZED_BOT.getBotBehaviour())
						.message("Request per minute exceeded the set threshold.").build()));
	}

	private void rateLimitSessionDuration(String domainCode, String ipAddress, String session)
			throws JsonProcessingException {
		ApolloProducer.publishMessage(KafkaTopicEnum.BLACKLIST.getKafkaTopic(), mapper.writeValueAsString(
				new BadBotInformationBuilder(domainCode, ServiceCodeEnum.WEB_AND_WEBAPI.getServiceCode(), ipAddress)
						.ruleName("ISR-STRMG-8").paramKey(RequestDataEnum.SESSION.getAttributeCode())
						.paramValue(session).botCategory(BotCategoryEnum.RATE_LIMITING.getBotCategory())
						.botClass(BotClassificationEnum.SESSION_LENGTH_EXCEEDED.getBotClassification())
						.botBehaviour(BotBehaviourEnum.UNCATEGORIZED_BOT.getBotBehaviour())
						.message("Session duration exceeded the set threshold.").build()));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer arg0) {
		// Auto-generated method stub
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public OutputCollector getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
	}

}
