package com.apollo.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class UriDetailsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String path;
	private String filename;

	public UriDetailsDto(String path, String filename) {
		super();
		this.path = path;
		this.filename = filename;
	}
}
