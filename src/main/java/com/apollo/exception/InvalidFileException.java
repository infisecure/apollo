package com.apollo.exception;

import com.apollo.constants.ErrorCodeEnum;

public class InvalidFileException extends Exception {

	private static final long serialVersionUID = 1L;

	private final ErrorCodeEnum code;

	public InvalidFileException(ErrorCodeEnum code) {
		super();
		this.code = code;
	}

	public InvalidFileException(String message, Throwable cause, ErrorCodeEnum code) {
		super(message, cause);
		this.code = code;
	}

	public InvalidFileException(String message, ErrorCodeEnum code) {
		super(message);
		this.code = code;
	}

	public InvalidFileException(Throwable cause, ErrorCodeEnum code) {
		super(cause);
		this.code = code;
	}

	public ErrorCodeEnum getCode() {
		return code;
	}
}
