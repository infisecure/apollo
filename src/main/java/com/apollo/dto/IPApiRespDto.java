package com.apollo.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class IPApiRespDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String query;
	private String status;
	private String as;
	private String city;
	private String region;
	private String regionName;
	private String country;
	private String countryCode;
	private String isp;
	private String org;
	private String mobile;
	private String proxy;
	private String reverse;
	private String message;

}
