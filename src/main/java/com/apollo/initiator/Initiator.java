package com.apollo.initiator;

import java.util.Map;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.Constant;
import com.apollo.constants.RequestCategoryEnum;
import com.apollo.storm.StormSpoutConfig;
import com.apollo.storm.bolts.AffirmCrawlerComponent;
import com.apollo.storm.bolts.BlacklistedComponent;
import com.apollo.storm.bolts.CrawlerComponent;
import com.apollo.storm.bolts.DataCenterComponent;
import com.apollo.storm.bolts.EmptyIPComponent;
import com.apollo.storm.bolts.FilteredRequestAllowedComponent;
import com.apollo.storm.bolts.HumanComponent;
import com.apollo.storm.bolts.ProxyComponent;
import com.apollo.storm.bolts.ReservedIPComponent;
import com.apollo.storm.bolts.RoutingBolt;
import com.apollo.storm.bolts.TorExitNodeComponent;

public class Initiator {
	private static final Logger LOGGER = LoggerFactory.getLogger(Initiator.class);

	private static final String STORM_YAML_FILE = "apollo-storm.yaml";

	private static final String TOPOLOGY = "apollo";
	private static final String SPOUT = "kafka-spout";

	private static final String ROUTING_BOLT = "routingBolt";

	private static final String EMPTY_IP_COMPONENT = "empty-ip-component";
	private static final String RESERVED_IP_COMPONENT = "reserved-ip-component";
	private static final String HUMAN_COMPONENT = "human-component";
	private static final String CRAWLER_COMPONENT = "crawler-component";
	private static final String BLACKLIST_COMPONENT = "blacklist-component";
	private static final String PROXY_COMPONENT = "proxy-component";
	private static final String TOR_COMPONENT = "tor-exit-node-component";
	private static final String DATACENTER_COMPONENT = "datacenter-component";
	private static final String AFFIRM_CRAWLER_COMPONENT = "affirm-crawler";
	private static final String FILTERED_REQUEST_ALLOWED_COMPONENT = "filtered-requests-allowed-component";

	public static void main(String[] args) throws Exception {
		LOGGER.info("Initiating topology definition process.");

		/** Topology Definition */
		TopologyBuilder topologyBuilder = new TopologyBuilder();
		topologyBuilder
				.setSpout(SPOUT,
						new KafkaSpout(StormSpoutConfig.initialize(Constant.ZOOKEEPER_HOSTS, Constant.KAFKA_TOPIC,
								"/" + Constant.KAFKA_TOPIC, Constant.SPOUT_CLIENT_ID)),
						1)
				.setMemoryLoad(1024.0, 512.0).setMaxTaskParallelism(3);

		/**
		 * The spout and the bolts are connected using shuffleGroupings. This type of
		 * grouping tells Storm to send messages from the source node to target nodes in
		 * randomly distributed fashion.
		 */
		topologyBuilder.setBolt(ROUTING_BOLT, new RoutingBolt(), 4).shuffleGrouping(SPOUT).setMemoryLoad(3072.0)
				.setNumTasks(8).setCPULoad(20);

		topologyBuilder.setBolt(EMPTY_IP_COMPONENT, new EmptyIPComponent()).shuffleGrouping(ROUTING_BOLT,
				RequestCategoryEnum.EMPTY_IP.getRequestCategory());

		topologyBuilder.setBolt(RESERVED_IP_COMPONENT, new ReservedIPComponent()).shuffleGrouping(ROUTING_BOLT,
				RequestCategoryEnum.RESERVED_IP.getRequestCategory());

		topologyBuilder.setBolt(HUMAN_COMPONENT, new HumanComponent(), 4)
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.HUMAN.getRequestCategory()).setMemoryLoad(3072.0)
				.setNumTasks(8).setCPULoad(30);

		topologyBuilder.setBolt(CRAWLER_COMPONENT, new CrawlerComponent(), 2)
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.CRAWLER.getRequestCategory()).setMemoryLoad(3072.0)
				.setNumTasks(8);

		topologyBuilder.setBolt(BLACKLIST_COMPONENT, new BlacklistedComponent())
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.BLACKLISTED.getRequestCategory())
				.setMemoryLoad(3072.0).setNumTasks(8).setCPULoad(30);

		topologyBuilder.setBolt(PROXY_COMPONENT, new ProxyComponent())
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.PROXY_IP.getRequestCategory()).setMemoryLoad(3072.0)
				.setNumTasks(8).setCPULoad(30);

		topologyBuilder.setBolt(TOR_COMPONENT, new TorExitNodeComponent())
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.TOR_EXIT_NODES.getRequestCategory())
				.setMemoryLoad(3072.0).setNumTasks(8).setCPULoad(30);

		topologyBuilder.setBolt(DATACENTER_COMPONENT, new DataCenterComponent())
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.DATACENTER_IP.getRequestCategory())
				.setMemoryLoad(3072.0).setNumTasks(8).setCPULoad(30);

		topologyBuilder.setBolt(AFFIRM_CRAWLER_COMPONENT, new AffirmCrawlerComponent()).shuffleGrouping(ROUTING_BOLT,
				RequestCategoryEnum.AFFIRM_CRAWLER.getRequestCategory());

		topologyBuilder.setBolt(FILTERED_REQUEST_ALLOWED_COMPONENT, new FilteredRequestAllowedComponent())
				.shuffleGrouping(ROUTING_BOLT, RequestCategoryEnum.FILTERED_REQUEST_ALLOWED.getRequestCategory());

		Map<String, Object> config = Utils.findAndReadConfigFile(STORM_YAML_FILE);

		/** Create Config instance for Storm cluster and Storm topology configuration */
		Config cfg = new Config();
		cfg.setNumWorkers(8);
		cfg.setNumAckers(8);
		cfg.setMaxSpoutPending(1500);
		cfg.setTopologyWorkerMaxHeapSize(8192.0);
		cfg.setTopologyStrategy(
				org.apache.storm.scheduler.resource.strategies.scheduling.DefaultResourceAwareStrategy.class);
		cfg.registerMetricsConsumer(org.apache.storm.metric.LoggingMetricsConsumer.class);
		cfg.putAll(config);

		String topology = null;
		if (args[0].length() < 0)
			topology = TOPOLOGY;
		else
			topology = args[0];

		LOGGER.info("Submiting a topology to run on the cluster with a progress bar. Topology Name - {}.", TOPOLOGY);
		StormSubmitter.submitTopologyWithProgressBar(topology, cfg, topologyBuilder.createTopology());
		LOGGER.info("Success!");

		/**
		 * // Run topology LocalCluster cluster = new LocalCluster();
		 * cluster.submitTopology("KafkaStormSample", cfg,
		 * topologyBuilder.createTopology());
		 */
	}
}
