package com.apollo.dto;

import java.io.Serializable;
import java.util.Date;

public class HumanIpSummaryElasticDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String isarips1;
	private String isarips2;
	private String isarips3;
	private String isarips4;
	private String isarips5;
	private String isarips6;
	private String isarips7;
	private String isarips8;
	private Date isarips9;
	private int isarips10;
	private int isarips11;
	private int isarips12;
	private Date isarips13;
	private long isarips14;

	private HumanIpSummaryElasticDto(HumanIpSummaryElasticDtoBuilder humanIpSummaryElasticDtoBuilder) {
		super();
		this.isarips1 = humanIpSummaryElasticDtoBuilder.isarips1;
		this.isarips2 = humanIpSummaryElasticDtoBuilder.isarips2;
		this.isarips3 = humanIpSummaryElasticDtoBuilder.isarips3;
		this.isarips4 = humanIpSummaryElasticDtoBuilder.isarips4;
		this.isarips5 = humanIpSummaryElasticDtoBuilder.isarips5;
		this.isarips6 = humanIpSummaryElasticDtoBuilder.isarips6;
		this.isarips7 = humanIpSummaryElasticDtoBuilder.isarips7;
		this.isarips8 = humanIpSummaryElasticDtoBuilder.isarips8;
		this.isarips9 = humanIpSummaryElasticDtoBuilder.isarips9;
		this.isarips10 = humanIpSummaryElasticDtoBuilder.isarips10;
		this.isarips11 = humanIpSummaryElasticDtoBuilder.isarips11;
		this.isarips12 = humanIpSummaryElasticDtoBuilder.isarips12;
		this.isarips13 = humanIpSummaryElasticDtoBuilder.isarips13;
		this.isarips14 = humanIpSummaryElasticDtoBuilder.isarips14;
	}

	public String getIsarips1() {
		return isarips1;
	}

	public String getIsarips2() {
		return isarips2;
	}

	public String getIsarips3() {
		return isarips3;
	}

	public String getIsarips4() {
		return isarips4;
	}

	public String getIsarips5() {
		return isarips5;
	}

	public String getIsarips6() {
		return isarips6;
	}

	public String getIsarips7() {
		return isarips7;
	}

	public String getIsarips8() {
		return isarips8;
	}

	public Date getIsarips9() {
		return isarips9;
	}

	public int getIsarips10() {
		return isarips10;
	}

	public int getIsarips11() {
		return isarips11;
	}

	public int getIsarips12() {
		return isarips12;
	}

	public Date getIsarips13() {
		return isarips13;
	}

	public long getIsarips14() {
		return isarips14;
	}

	public static class HumanIpSummaryElasticDtoBuilder {
		private String isarips1;
		private String isarips2;
		private String isarips3;
		private String isarips4;
		private String isarips5;
		private String isarips6;
		private String isarips7;
		private String isarips8;
		private Date isarips9;
		private int isarips10;
		private int isarips11;
		private int isarips12;
		private Date isarips13;
		private long isarips14;

		public HumanIpSummaryElasticDtoBuilder(String domainCode, String ipAddress) {
			this.isarips1 = domainCode;
			this.isarips2 = ipAddress;
		}

		public HumanIpSummaryElasticDtoBuilder ipDetails(IPApiRespDto ipApiRespDto) {
			this.isarips3 = ipApiRespDto.getIsp();
			this.isarips4 = ipApiRespDto.getRegion();
			this.isarips5 = ipApiRespDto.getRegionName();
			this.isarips6 = ipApiRespDto.getCity();
			this.isarips7 = ipApiRespDto.getCountryCode();
			this.isarips8 = ipApiRespDto.getCountry();
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder startTime(Date startTime) {
			this.isarips9 = startTime;
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder minute(int minute) {
			this.isarips10 = minute;
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder hour(int hour) {
			this.isarips11 = hour;
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder dayOfWeek(int dayOfWeek) {
			this.isarips12 = dayOfWeek;
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder endTime(Date endTime) {
			this.isarips13 = endTime;
			return this;
		}

		public HumanIpSummaryElasticDtoBuilder numberOfHits(int numberOfHits) {
			this.isarips14 = numberOfHits;
			return this;
		}

		public HumanIpSummaryElasticDto build() {
			return new HumanIpSummaryElasticDto(this);
		}
	}

}
