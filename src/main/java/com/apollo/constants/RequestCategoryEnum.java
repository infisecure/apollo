package com.apollo.constants;

public enum RequestCategoryEnum {

	HUMAN("HUMAN"), RESERVED_IP("RESERVED_IP"), EMPTY_IP("EMPTY_IP"), CRAWLER("CRAWLER"), BLACKLISTED("BLACKLISTED"),
	PROXY_IP("PROXY_IP"), TOR_EXIT_NODES("TOR_EXIT_NODE"), DATACENTER_IP("DATACENTER_AND_WEB_HOSTING"),
	AFFIRM_CRAWLER("AFFIRM_CRAWLER"), FILTERED_REQUEST_ALLOWED("FILTERED_REQUEST_ALLOWED"),
	UNAUTHORIZED_REQUEST("UNAUTHORIZED_REQUEST");

	private String requestCategory;

	private RequestCategoryEnum(final String requestCategory) {
		this.requestCategory = requestCategory;
	}

	@Override
	public String toString() {
		return this.requestCategory;
	}

	public String getName() {
		return this.name();
	}

	public String getRequestCategory() {
		return requestCategory;
	}

}
