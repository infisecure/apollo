package com.apollo.util;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLInjectionFilter {

	private SQLInjectionFilter() {
		super();
	}

	private static final String[] KEYWORDS_TO_CHECK = { "select", "drop", "from", "exec", "exists", "update", "delete",
			"insert", "cast", "http", "sql", "null", "like", "mysql", "()", "information_schema", "sleep", "version",
			"join", "declare", "having", "signed", "alter", "union", "where", "create", "shutdown", "grant",
			"privileges" };

	private static final String SQL_TYPES = "TABLE, TABLESPACE, PROCEDURE, FUNCTION, TRIGGER, KEY, VIEW, MATERIALIZED VIEW, LIBRARY, "
			+ "DATABASE LINK, DBLINK, INDEX, CONSTRAINT, TRIGGER, USER, SCHEMA, DATABASE, PLUGGABLE DATABASE, "
			+ "BUCKET, CLUSTER, COMMENT, SYNONYM, TYPE, JAVA, SESSION, ROLE, PACKAGE, PACKAGE BODY, OPERATOR, "
			+ "SEQUENCE, RESTORE POINT, PFILE, CLASS, CURSOR, OBJECT, RULE, USER, DATASET, DATASTORE, "
			+ "COLUMN, FIELD, OPERATOR";

	private static final String[] SQL_REGEXPS = { "(/\\*).*(\\*/)", "(--.*)$", ";+\"+\'", "\"{2,}+", "\'{2,}+",
			"\\d=\\d", "(\\s\\s)+", "%{2,}+", "([;\'\"\\=]+.*(admin.*))|((admin.*).*[;\'\"\\=]+)", "%+[0-7]+[0-9|A-F]+",
			"(?i)(.*)(\\b)+SELECT(\\b)+\\s.*(\\b)+FROM(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+INSERT(\\b)+\\s.*(\\b)+INTO(\\b)+\\s.*(.*)", "(?i)(.*)(\\b)+UPDATE(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+DELETE(\\b)+\\s.*(\\b)+FROM(\\b)+\\s.*(.*)", "(?i)(.*)(\\b)+UPSERT(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+SAVEPOINT(\\b)+\\s.*(.*)", "(?i)(.*)(\\b)+CALL(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+ROLLBACK(\\b)+\\s.*(.*)", "(?i)(.*)(\\b)+KILL(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+DROP(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+CREATE(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+ALTER(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+TRUNCATE(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+LOCK(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+UNLOCK(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+RELEASE(\\b)+(\\s)*(" + SQL_TYPES.replaceAll(",", "|") + ")(\\b)+\\s.*(.*)",
			"(?i)(.*)(\\b)+DESC(\\b)+(\\w)*\\s.*(.*)", "(?i)(.*)(\\b)+DESCRIBE(\\b)+(\\w)*\\s.*(.*)", "(.*)(-){2,}(.*)",
			"[<>]" };

	private static final List<Pattern> validationPatterns = new ArrayList<>();
	static {
		List<Pattern> patterns = new ArrayList<>();
		for (String sqlExpression : SQL_REGEXPS) {
			patterns.add(getPattern(sqlExpression.toLowerCase()));
		}
		validationPatterns.addAll(patterns);
	}

	public static boolean isSqlInjectionSafe(String dataString) {
		dataString = Normalizer.normalize(dataString, Form.NFKC);
		return (isSqlInjectionSafeKeyword(dataString) && isSqlInjectionSafeRegex(dataString));
	}

	private static boolean isSqlInjectionSafeKeyword(final String dataString) {
		String input = dataString;
		for (String keyword : KEYWORDS_TO_CHECK) {
			if (input.toLowerCase().contains(keyword))
				return false;
		}
		return true;
	}

	private static boolean isSqlInjectionSafeRegex(final String dataString) {
		String input = dataString;
		for (Pattern pattern : validationPatterns) {
			if (matches(pattern, input))
				return false;
		}
		return true;
	}

	private static boolean matches(Pattern pattern, String dataString) {
		Matcher matcher = pattern.matcher(dataString);
		return matcher.find();

	}

	private static Pattern getPattern(String regEx) {
		return Pattern.compile(regEx, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
	}
}
