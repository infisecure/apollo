package com.apollo.constants;

import java.math.BigInteger;

import com.apollo.util.ReadProperties;

public class Constant {

	private Constant() {
		super();
	}

	public static final String EMPTY_STRING = "";
	public static final String COLON_DELIMITER = ":";

	public static final String AJAX_REQUEST_IDENTIFIER = "XMLHttpRequest";

	/** ---------- Blacklist Map Keys ---------- */
	public static final String BLACKLISTED_PARAM_KEY = "blacklisted_param";
	public static final String BLACKLISTED_VALUE_KEY = "blacklisted_value";
	public static final String BOT_CLASSIFICATION_KEY = "bot_classification";

	public static final String CRAWLER_CATEGORY_KEY = "crawler_category";
	public static final String CRAWLER_FAMILY_KEY = "crawler_family";
	public static final String CRAWLER_NAME_KEY = "crawler_name";
	public static final String CRAWLER_IDENTIFICATION_KEY = "identification_code";

	public static final String TOTAL_REQUEST = "total_request";
	public static final String TOTAL_NON_AJAX_REQUEST = "total_non_ajax_request";
	public static final String SAME_REQUEST_REFERER = "same_req_ref";

	/** ---------- IP Related Details ---------- **/
	public static final String LOW_DECIMAL_KEY = "low_decimal";
	public static final String HIGH_DECIMAL_KEY = "high_decimal";
	public static final Double MAX_IPV4_RANGE = 4294967295d;
	public static final BigInteger MAX_IPV6_RANGE = new BigInteger("340282366920938463463374607431768211455");

	/** ---------- Redis Keys ---------- **/
	public static final String API_KEY = "K:APIKEY";

	/** ---------- Redis Hash Keys ---------- **/
	public static final String DOMAIN_HASH_KEY = "H:DOMAIN";
	public static final String REQUEST_DETAILS_HASH_KEY = "H:REQUEST_DETAILS";
	public static final String ACL_CONFIG_RATE_LIMITING_HASH_KEY = "H:ACL_CONFIG:RATE_LIMITING";
	public static final String BLACKLIST_CONFIG_HASH_KEY = "H:BLACKLIST_CONFIG";
	public static final String BLACKLISTED_HASH_KEY = "H:BAD";
	public static final String CRAWLER_HASH_KEY = "H:CRAWLER:";
	public static final String CRAWLER_DOMAIN_HASH_KEY = "H:CRAWLER:GOOD_DOMAIN";
	public static final String CRAWLER_ISP_HASH_KEY = "H:CRAWLER:GOOD_ISP";
	public static final String CRAWLING_PATTERN_KEY = "H:CRAWLING_PATTERN";
	public static final String CRAWLER_VALIDATION_CHECKS_HASH_KEY = "H:CRAWLER_VALIDATION_CHECKS";
	public static final String CRAWLER_UA_CODE_MAP_KEY = "H:CRAWLER_UA:CODE";
	public static final String CRAWLER_DETAILS_HASH_KEY = "H:CRAWLER_IDENTITY";
	public static final String DC_CIDR_DETAIL_HASH_KEY = "H:DATACENTER_CIDR";
	public static final String CRAWLER_CIDR_DETAIL_HASH_KEY = "H:CRAWLER_CIDR";
	public static final String NON_STANDARD_UA_HASH_KEY = "H:NON_STANDARD_UA_BUILD";

	public static final String REQUEST_PER_MIN_HASH_KEY = "L:REQ_PER_MIN";

	/** ---------- Redis Sorted Set Keys ---------- **/
	public static final String CRAWLER_SORTED_SET_KEY = "Z:XENIOS:IP_TABLE:CRAWLER";

	/** ---------- Kafka and Storm Config Details ---------- **/
	public static final String KAFKA_TOPIC = ReadProperties.getProperty("source.topic");
	public static final String ZOOKEEPER_HOSTS = ReadProperties.getProperty("zookeeper.connect");

	public static final String SPOUT_CLIENT_ID = ReadProperties.getProperty("spout.client.id");
	public static final int SPOUT_BUFFER_SIZE = Integer
			.parseInt(ReadProperties.getProperty("spout.buffer.size").trim());
	public static final int SPOUT_FETCH_BYTE_SIZE = Integer
			.parseInt(ReadProperties.getProperty("spout.fetch.byte.size").trim());
	public static final int SPOUT_MIN_FETCH_BYTE_SIZE = Integer
			.parseInt(ReadProperties.getProperty("spout.min.fetch.byte.size").trim());
	public static final int SPOUT_FETCH_MAX_WAIT = Integer
			.parseInt(ReadProperties.getProperty("spout.fetch.max.wait").trim());
	public static final int SPOUT_SOCKET_TIMEOUT_MS = Integer
			.parseInt(ReadProperties.getProperty("spout.socket.timeout").trim());

	/** ---------- Redis Connection Details ---------- **/
	public static final String REDIS_HOST = ReadProperties.getProperty("redis.host");
	public static final int REDIS_PORT = Integer.parseInt(ReadProperties.getProperty("redis.port"));
	public static final String REDIS_PASSWORD = ReadProperties.getProperty("redis.password");

}
