package com.apollo.kafka;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.util.ErrorHandler;
import com.apollo.util.StringUtil;

public class ApolloProducer {

	private ApolloProducer() {
		super();
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(ApolloProducer.class);
	private static final String KAFKA_CONFIG_FILE = "kafka_config.properties";

	private static Properties properties;
	private static Producer<String, String> producer;

	static {
		InputStream input = ApolloProducer.class.getClassLoader().getResourceAsStream(KAFKA_CONFIG_FILE);
		if (input != null) {
			properties = new Properties();
			try {
				properties.load(input);
			} catch (IOException e) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				ErrorHandler.sendErrorMail(sw.toString(),
						"Exception while loading kafka properties file, " + KAFKA_CONFIG_FILE);
				throw new IllegalArgumentException(java.text.MessageFormat
						.format("The properties file could not be loaded, {0}", KAFKA_CONFIG_FILE), e);
			}
			producer = new KafkaProducer<>(properties);
		} else {
			LOGGER.error(
					"Sorry, unable to find {}. Please verify the file name and check if the file is available in classpath.",
					KAFKA_CONFIG_FILE);
		}
	}

	public static void publishMessage(final String topic, final String tuple) {
		String topicName = topic;
		String dataTuple = tuple;
		if (StringUtil.isEmpty(dataTuple) || StringUtil.isEmpty(topicName)) {
			LOGGER.error("Topic or tuple cannot be null or empty!");
			return;
		}
		producer.send(new ProducerRecord<String, String>(topicName, dataTuple), new ApolloCallback(dataTuple));
	}
}
