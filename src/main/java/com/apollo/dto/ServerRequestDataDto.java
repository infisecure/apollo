package com.apollo.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ServerRequestDataDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String isar1;
	private String isar2;
	private String isar3;
	private String isar4;
	private String isar5;
	private String isar6;
	private String isar7;
	private String isar8;
	private String isar9;
	private String isar10;
	private String isar11;
	private String isar12;
	private String isar13;
	private String isar14;
	private String isar15;
	private String isar16;
	private String isar17;
	private String isar18;
	private String isar19;
	private String isar20;
	private String isar21;
	private String isar22;
	private String isar23;
	private String isar24;
	private String isar25;
	private String isar26;
	private String isar27;
	private String isar28;
	private String isar29;
	private String isar30;
	private String isar31;
	private String isar32;
	private String isar33;
	private String isar34;
	private String isar35;
	private String isar36;
	private String isar37;
	private String isar38;
	private String isar39;
	private String isar40;

}
