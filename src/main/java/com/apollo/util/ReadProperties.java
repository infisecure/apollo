package com.apollo.util;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ReadProperties {

	private ReadProperties() {
	}

	public static final Logger LOGGER = Logger.getLogger(ReadProperties.class);
	private static final String APOLLO_PROPERTIES = "apollo.properties";

	public static String getProperty(String propertyName) {
		Properties properties = new Properties();
		String propertyValue = "";
		try (InputStream inputStream = ReadProperties.class.getClassLoader().getResourceAsStream(APOLLO_PROPERTIES)) {
			properties.load(inputStream);
			propertyValue = properties.getProperty(propertyName);
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			LOGGER.error("Exception reading properties file, " + sw.toString());
		}
		return propertyValue;
	}
}
