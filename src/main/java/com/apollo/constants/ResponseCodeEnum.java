package com.apollo.constants;

public enum ResponseCodeEnum {

	ALLOW("1000"), CAPTCHA("1001"), FEED_FAKE_DATA("1002"), BLOCK("1003"), DROP("1004"), MONITOR("1005"),
	EXCEPTION("-1"), STATIC_RESOURCE("-2"), SUBSCRIPTION_EXPIRED("1011"), UNAUTHORIZED_ACCESS("1012");

	private String responseCode;

	private ResponseCodeEnum(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	@Override
	public String toString() {
		return responseCode;
	}

	public String getName() {
		return this.name();
	}

}
