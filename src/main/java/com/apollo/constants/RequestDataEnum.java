package com.apollo.constants;

public enum RequestDataEnum {

	DOMAIN_CODE("isar1", "Domain Code", "InfiSecure assigned Domain Code to uniquely identify a domain."),
	REQUEST_ID("isar2", "Request ID",
			"InfiSecure computed ID for the request. Identified as the Request Id on error pages and is used to identify the request uniquely."),
	REFERER_URL("isar3", "Referer Url",
			"Visitor provided HTTP referrer. Referrer: An HTTP header field that identifies the address of the webpage (i.e. the URI or IRI) that linked to the resource being requested. By checking the referrer, the new webpage can see where the request originated."),
	REQUESTED_URL("isar4", "requested url", "URL requested by the visitor"),
	QUERY_STRING("isar5", "Query String", "Query String:  Client HTTP request query parameters."),
	REQUEST_METHOD("isar6", "HTTP Request Method",
			"HTTP Request Method: HTTP request method name GET, POST, PUT, etc."),
	USER_ID("isar7", "User ID",
			"Requested By: User ID (or hashed user id) that can be identified uniquely in subscriber application environment."),
	REQUEST_TIME("isar8", "Request Time", "Time of the request in milliseconds"),
	IP_ADDRESS("isar9", "IP Address", "Visitor’s IP address"),
	USER_AGENT("isar10", "User Agent",
			"Visitor-provided HTTP user agent. User Agent: Is a software that is acting on behalf of a user. One common use of the term refers to a web browser telling a website information about the browser and operating system."),
	SESSION("isar11", "Session ID",
			"Session ID: A piece of data that is used over HTTP to identify a session, a series of related message exchanges. "),
	IS_COOKIE_A("isar12", "__utm_is1",
			"Infisecure cookie __utm_is1 value. Infisecure’s cookie with an expiry of one year from the latest request made by a same browser."),
	IS_COOKIE_B("isar13", "__utm_is2",
			"Infisecure cookie __utm_is2 value. Infisecure’s cookie with an expiry of 3 hrs from the latest request made by a same browser. The value of this cookie denotes the timestamp of the first request made by browser."),
	IS_COOKIE_C("isar14", "__utm_is3",
			"Infisecure cookie __utm_is3 value. Infisecure’s cookie with an expiry of 3 hrs from last request by a same browser. The value of this cookie is unique for every request that is made by browser. Used to identify the browsing brhaviour of a user."),
	IS_COOKIE_D("isar15", "__utm_is_wdck",
			"InfiSecure cookie __utm_is_wdck value. Infisecure’s cookie with an expiry of 3 hrs from last request by a same browser."),
	DEVICE_ID("isar16", "Device ID",
			"Infisecure cookie __utm_is_did value. Infisecure’s cookie with an expiry of 3 hrs from last request by a same browser. The value of this cookie is the client browser fingerprint ID that has been sent from browser for every request that it has made."),
	HEADERS_LIST("isar17", "Headers List",
			"Headers List: List of all header names sent by the client in received order, comma separated. Ex: Host, Connection, Pragma, Cookie, Cache-Control"),
	ORIGIN("isar18", "HTTP Header Origin", "Origin: Value of HTTP request header \"Origin\""),
	HOST("isar19", "HTTP Header Host", "Host: Value of HTTP request header \"Host\""),
	X_FORWARDED_FOR("isar20", "HTTP Header X-Forwarded-For",
			"X-Forwarded-For: Value of HTTP request header \"X-Forwarded-For\""),
	X_REQUESTED_WITH("isar21", "HTTP Header X-Requested-With",
			"X-Requested-With: Value of HTTP request header \"X-Requested-With\""),
	ACCEPT("isar22", "HTTP Header Accept", "Accept: Value of HTTP request header \"Accept\""),
	ACCEPT_CHARSET("isar23", "HTTP Header Accept-Charset",
			"Accept-Charset: Value of HTTP request header \"Accept-Charset\""),
	ACCEPT_ENCCODING("isar24", "HTTP Header Accept-Encoding",
			"Accept-Encoding: Value of HTTP request header \"Accept-Encoding\""),
	ACCEPT_LANGUAGE("isar25", "HTTP Header Accept-Language",
			"Accept-Language: Value of HTTP request header \"Accept-Language\""),
	PRAGMA("isar26", "HTTP Header Pragma", "Pragma: Value of HTTP request header \"Pragma\""),
	CACHE_CONTROL("isar27", "HTTP Header Cache-Control",
			"Cache-Control: Value of HTTP request header \"Cache-Control\""),
	CONNECTION("isar28", "HTTP Header Connection", "Connection: Value of HTTP request header \"Connection\""),
	SERVER_SERIAL("isar29", "Server Serial",
			"Server Serial: The name of the server which is accepting and processing the HTTP request made by browser."),
	REQUEST_PROTOCOL("isar30", "Request Protocol",
			"Indicates whether the request was made to InfiSecure using HTTP or HTTPS"),
	REQUEST_CATEGORY("isar31", "Request Category", "Request Category."),
	SUBSCRIBER_CODE("isar32", "Subscriber Code", "Subscriber Code."),
	IS_MACHINE_ID("isar33", "InfiSecure Machine ID", "Id of the machine that processed the API request."),
	SECURITY_TYPE("isar34", "Security Type", "Security type web or mobile web."),
	PROCESSING_TIME("isar35", "Request Processing Time", "Time when the request is being processed."),
	INFISECURE_ACTION("isar36", "InfiSecure Action", "Code of the action InfiSecure took on the request"),
	BOT_CATEGORY("isar37", "Bot Category", "Bot Category.");

	private String attributeCode;
	private String attributeName;
	private String description;

	private RequestDataEnum(String attributeCode, String attributeName, String description) {
		this.attributeCode = attributeCode;
		this.attributeName = attributeName;
		this.description = description;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public String getDescription() {
		return description;
	}

}
