/**
 * This utility provides methods to work with IPv4 address.
 *
 * @author Ashish
 */
package com.apollo.util;

public class IPv4Util {

	private static final int MAX_CIDR = 32;

	/**
	 * Returns the long format of the provided IP address.
	 *
	 * @param ipAddress the IP address
	 * @return the long format of <code>ipAddress</code>
	 * @throws IllegalArgumentException if <code>ipAddress</code> is invalid
	 */
	public static long toLongIpv4(String ipAddress) {
		if (ipAddress == null || ipAddress.isEmpty()) {
			throw new IllegalArgumentException("ip address cannot be null or empty");
		}
		String[] octets = ipAddress.split(java.util.regex.Pattern.quote("."));
		if (octets.length != 4) {
			throw new IllegalArgumentException("invalid ip address");
		}
		long ip = 0;
		for (int i = 3; i >= 0; i--) {
			long octet = Long.parseLong(octets[3 - i]);
			if (octet > 255 || octet < 0) {
				throw new IllegalArgumentException("invalid ip address");
			}
			ip |= octet << (i * 8);
		}
		return ip;
	}

	/**
	 * Returns the 32bit dotted format of the provided long ip.
	 *
	 * @param ip the long ip
	 * @return the 32bit dotted format of <code>ip</code>
	 * @throws IllegalArgumentException if <code>ip</code> is invalid
	 */
	public static String long2Ip(long ip) {
		// if ip is bigger than 255.255.255.255 or smaller than 0.0.0.0
		if (ip > 4294967295l || ip < 0) {
			throw new IllegalArgumentException("invalid ip");
		}
		StringBuilder ipAddress = new StringBuilder();
		for (int i = 3; i >= 0; i--) {
			int shift = i * 8;
			ipAddress.append((ip & (0xff << shift)) >> shift);
			if (i > 0) {
				ipAddress.append(".");
			}
		}
		return ipAddress.toString();
	}

	public static boolean isNetworksOverlap(final String cidrA, final String cidrB) {
		try {
			final Long[] cidrALong = cidrToLong(cidrA);
			final Long[] cidrBLong = cidrToLong(cidrB);
			final long shift = MAX_CIDR - (cidrALong[1] > cidrBLong[1] ? cidrBLong[1] : cidrALong[1]);
			return cidrALong[0] >> shift == cidrBLong[0] >> shift;
		} catch (final Exception e) {
//			s_logger.error(e.getLocalizedMessage(), e);
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isNetworkAWithinNetworkB(final String cidrA, final String cidrB) {
		if (!areCidrsNotEmpty(cidrA, cidrB)) {
			return false;
		}
		final Long[] cidrALong = cidrToLong(cidrA);
		final Long[] cidrBLong = cidrToLong(cidrB);

		final long shift = MAX_CIDR - cidrBLong[1];
		return (cidrALong[0] >> shift == cidrBLong[0] >> shift) && (cidrALong[1] >= cidrBLong[1]);
	}

	public static boolean areCidrsNotEmpty(final String cidrA, final String cidrB) {
		return StringUtil.isNotEmpty(cidrA) && StringUtil.isNotEmpty(cidrB);
	}

	public static Long[] cidrToLong(final String cidr) {
		if (cidr == null || cidr.isEmpty()) {
			throw new IllegalArgumentException("Empty cidr can not be converted to longs.");
		}
		final String[] cidrPair = cidr.split("\\/");
		if (cidrPair.length != 2) {
			throw new IllegalArgumentException("CIDR is not formatted correctly: " + cidr);
		}
		final String cidrAddress = cidrPair[0];
		final String cidrSize = cidrPair[1];

		final long cidrSizeNum = getCidrSizeFromString(cidrSize);
		final long numericNetmask = netMaskFromCidr(cidrSizeNum);
		final long ipAddr = toLongIpv4(cidrAddress);
		final Long[] cidrlong = { ipAddr & numericNetmask, cidrSizeNum };
		return cidrlong;
	}

	/**
	 * @param cidrSize
	 * @return
	 * @throws CloudRuntimeException
	 */
	static long getCidrSizeFromString(final String cidrSize) {
		long cidrSizeNum = -1;

		try {
			cidrSizeNum = Integer.parseInt(cidrSize);
		} catch (final NumberFormatException e) {
//			throw new CloudRuntimeException("cidrsize is not a valid int: " + cidrSize, e);
			e.printStackTrace();
		}
		if (cidrSizeNum > 32 || cidrSizeNum < 0) {// assuming IPv4
			throw new IllegalArgumentException("CIDR size out of range: " + cidrSizeNum);
		}
		return cidrSizeNum;
	}

	/**
	 * @param cidrSize
	 * @return
	 */
	static long netMaskFromCidr(final long cidrSize) {
		return ((long) 0xffffffff) >> MAX_CIDR - cidrSize << MAX_CIDR - cidrSize;
	}

	public static void main(String[] args) {

		System.out.println(System.currentTimeMillis());
		System.out.println(toLongIpv4("81.19.188.238"));
		System.out.println(System.currentTimeMillis());

		System.out.println(isNetworksOverlap("106.51.1.0/22", "106.51.0.0/22"));
		System.out.println(isNetworkAWithinNetworkB("106.51.0.0/22", "106.51.1.0/22"));

	}

}
