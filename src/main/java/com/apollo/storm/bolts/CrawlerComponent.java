package com.apollo.storm.bolts;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.Constant;
import com.apollo.constants.KafkaTopicEnum;
import com.apollo.constants.RequestCategoryEnum;
import com.apollo.dto.CrawlerElasticDataDto;
import com.apollo.dto.CrawlerElasticDataDto.CrawlerElasticDataBuilder;
import com.apollo.dto.ServerRequestDataDto;
import com.apollo.kafka.ApolloProducer;
import com.apollo.storm.jedis.JedisConnectionPool;
import com.apollo.util.StringUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;

public class CrawlerComponent extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(CrawlerComponent.class);

	private ObjectMapper mapper;
	private transient OutputCollector outputCollector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map map, TopologyContext context, OutputCollector collector) {
		LOGGER.info("Preparing Crawler Component Task Bolt.");
		int totalTasks = context.getComponentTasks(RequestCategoryEnum.CRAWLER.getRequestCategory()).size();
		LOGGER.info("Total Task, {}", totalTasks);
		this.outputCollector = collector;
		this.mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
	}

	@Override
	public void execute(Tuple tuple) {
		/** get input data from the tuple */
		String inputData = tuple.getString(0);
		Jedis jedis = null;

		if (StringUtil.isNotEmpty(inputData)) {
			LOGGER.info("Processing Crawler API data, {}", inputData);
			ServerRequestDataDto requestDataDto = null;
			try {
				/** convert json string to request object */
				requestDataDto = mapper.readValue(inputData, ServerRequestDataDto.class);

				/**
				 * convert the time stamp to date and get minute, hour and day of week
				 */
				long requestTime = Long.parseLong(requestDataDto.getIsar8());
				Date apiHitTime = new Date(requestTime);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(apiHitTime);
				int minute = calendar.get(Calendar.MINUTE);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

				long processingTime = Long.parseLong(requestDataDto.getIsar35()) - requestTime;

				/** check if the crawler key is already present */
				jedis = JedisConnectionPool.getJedisConnection();

				/**
				 * create crawler user agent details key H:CRAWLER:<USERAGENT>
				 */
				String userAgent = StringUtil.getNotNullValue(requestDataDto.getIsar10());
				String crawlerCode = jedis.hget(Constant.CRAWLER_UA_CODE_MAP_KEY, userAgent);
				String crawlerDetailsKey = new StringBuilder(Constant.CRAWLER_DETAILS_HASH_KEY)
						.append(Constant.COLON_DELIMITER).append(crawlerCode).toString();

				Map<String, String> crawlerDetailsMap = jedis.hgetAll(crawlerDetailsKey);
				CrawlerElasticDataDto crawlerDataDto = new CrawlerElasticDataBuilder(requestDataDto).minute(minute)
						.hour(hour).dayOfWeek(dayOfWeek)
						.crawlerDetails(crawlerDetailsMap.get(Constant.CRAWLER_NAME_KEY),
								crawlerDetailsMap.get(Constant.CRAWLER_FAMILY_KEY),
								crawlerDetailsMap.get(Constant.CRAWLER_CATEGORY_KEY),
								crawlerDetailsMap.get(Constant.CRAWLER_IDENTIFICATION_KEY))
						.processingTimeDifference(processingTime).build();

				// write to crawler topic
				ApolloProducer.publishMessage(KafkaTopicEnum.GOOD_BOTS.getKafkaTopic(),
						mapper.writeValueAsString(crawlerDataDto));
				outputCollector.ack(tuple);
			} catch (IOException e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception while parsing input data, {}", e);
			} catch (Exception e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception, {}", e);
			} finally {
				JedisConnectionPool.close(jedis);
			}
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer arg0) {
		// End of operation, the data is enriched and written to Kafka queue in
		// this bolt
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public OutputCollector getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
	}
}
