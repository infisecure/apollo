package com.apollo.util;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.apollo.constants.IPVersionEnum;
import com.apollo.dto.IPApiRespDto;

public class IPUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(IPUtil.class);

	private IPUtil() {
		super();
	}

	public static boolean isReservedAddress(String ipAddress) {
		boolean isReservedAddress = false;
		try {
			final InetAddress inetAddress = InetAddress.getByName(ipAddress);
			isReservedAddress = inetAddress.isLinkLocalAddress() || inetAddress.isAnyLocalAddress()
					|| inetAddress.isLoopbackAddress() || inetAddress.isSiteLocalAddress();
		} catch (UnknownHostException e) {
			LOGGER.error("Exception Checking Local IP Address, IP Address - {0}. Exception - {1}", ipAddress, e);
		}
		return isReservedAddress;
	}

	public static boolean isPublicAddress(final String address) {
		return !isReservedAddress(address);
	}

	public static boolean isValidIpAddress(String ipAddress) {
		return isIPv4(ipAddress) || isIPv6(ipAddress);
	}

	public static String getIPVersion(String ipAddress) {
		if (isIPv4(ipAddress))
			return IPVersionEnum.IPV4.getIpVersion();

		if (isIPv6(ipAddress))
			return IPVersionEnum.IPV6.getIpVersion();

		return "";
	}

	public static boolean isIPv4(String ipAddress) {
		boolean isIPv4 = false;
		if (ipAddress != null) {
			try {
				InetAddress inetAddress = InetAddress.getByName(ipAddress);
				isIPv4 = (inetAddress instanceof Inet4Address) && inetAddress.getHostAddress().equals(ipAddress);
			} catch (UnknownHostException e) {
				LOGGER.error("Exception Validating IPv4 Address, {}", e);
			}
		}
		return isIPv4;
	}

	public static boolean isIPv6(String ipAddress) {
		boolean isIPv6 = false;
		if (ipAddress != null) {
			try {
				InetAddress inetAddress = InetAddress.getByName(ipAddress);
				isIPv6 = (inetAddress instanceof Inet6Address);
			} catch (UnknownHostException e) {
				LOGGER.error("Exception Validating IPv6 Address, {}", e);
			}
		}
		return isIPv6;
	}

	public static IPApiRespDto getIpApiDetail(String ipAddress) {
		IPApiRespDto result;
		try {
			UriComponents uriComponents = UriComponentsBuilder
					.fromUriString("http://pro.ip-api.com/json/{ipAddress}?fields=261663&key=HWoTB7BFNos0snU").build()
					.expand(ipAddress).encode();
			URI uri = uriComponents.toUri();
			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.getForObject(uri, IPApiRespDto.class);
		} catch (Exception e) {
			result = new IPApiRespDto();
			result.setStatus("1001");
			result.setMessage("exceptin - " + e.getMessage());
		}
		return result;
	}

}
