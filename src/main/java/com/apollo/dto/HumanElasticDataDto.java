package com.apollo.dto;

import java.io.Serializable;
import java.util.Date;

import com.apollo.util.StringUtil;

import lombok.Data;

@Data
public class HumanElasticDataDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String AJAX_REQUEST_IDENTIFIER = "XMLHttpRequest";

	private HumanElasticDataDto() {
		super();
	}

	private String isar1;
	private String isar2;
	private String isar3;
	private String isar4;
	private String isar5;
	private String isar6;
	private String isar7;
	private String isar8;
	private String isar9;
	private String isar10;
	private Date isar11;
	private int isar12;
	private int isar13;
	private int isar14;
	private String isar15;
	private String isar16;
	private String isar17;
	private String isar18;
	private String isar19;
	private String isar20;
	private String isar21;
	private String isar22;
	private String isar23;
	private String isar24;
	private String isar25;
	private int isar26;
	private long isar27;
	private int isar28;
	private String isar29;
	private String isar30;
	private String isar31;
	private String isar32;
	private String isar33;
	private String isar34;
	private String isar35;
	private String isar36;
	private String isar37;
	private String isar38;
	private String isar39;
	private String isar40;
	private String isar41;
	private String isar42;
	private String isar43;
	private String isar44;
	private String isar45;
	private String isar46;
	private String isar47;
	private long isar48;
	private String isar49;

	private HumanElasticDataDto(HumanElasticDataBuilder humanElasticDataBuilder) {
		super();
		this.isar1 = humanElasticDataBuilder.isar1;
		this.isar2 = humanElasticDataBuilder.isar2;
		this.isar3 = humanElasticDataBuilder.isar3;
		this.isar4 = humanElasticDataBuilder.isar4;
		this.isar5 = humanElasticDataBuilder.isar5;
		this.isar6 = humanElasticDataBuilder.isar6;
		this.isar7 = humanElasticDataBuilder.isar7;
		this.isar8 = humanElasticDataBuilder.isar8;
		this.isar9 = humanElasticDataBuilder.isar9;
		this.isar10 = humanElasticDataBuilder.isar10;
		this.isar11 = humanElasticDataBuilder.isar11;
		this.isar12 = humanElasticDataBuilder.isar12;
		this.isar13 = humanElasticDataBuilder.isar13;
		this.isar14 = humanElasticDataBuilder.isar14;
		this.isar15 = humanElasticDataBuilder.isar15;
		this.isar16 = humanElasticDataBuilder.isar16;
		this.isar17 = humanElasticDataBuilder.isar17;
		this.isar18 = humanElasticDataBuilder.isar18;
		this.isar19 = humanElasticDataBuilder.isar19;
		this.isar20 = humanElasticDataBuilder.isar20;
		this.isar21 = humanElasticDataBuilder.isar21;
		this.isar22 = humanElasticDataBuilder.isar22;
		this.isar23 = humanElasticDataBuilder.isar23;
		this.isar24 = humanElasticDataBuilder.isar24;
		this.isar25 = humanElasticDataBuilder.isar25;
		this.isar26 = humanElasticDataBuilder.isar26;
		this.isar27 = humanElasticDataBuilder.isar27;
		this.isar28 = humanElasticDataBuilder.isar28;
		this.isar29 = humanElasticDataBuilder.isar29;
		this.isar30 = humanElasticDataBuilder.isar30;
		this.isar31 = humanElasticDataBuilder.isar31;
		this.isar32 = humanElasticDataBuilder.isar32;
		this.isar33 = humanElasticDataBuilder.isar33;
		this.isar34 = humanElasticDataBuilder.isar34;
		this.isar35 = humanElasticDataBuilder.isar35;
		this.isar36 = humanElasticDataBuilder.isar36;
		this.isar37 = humanElasticDataBuilder.isar37;
		this.isar38 = humanElasticDataBuilder.isar38;
		this.isar39 = humanElasticDataBuilder.isar39;
		this.isar40 = humanElasticDataBuilder.isar40;
		this.isar41 = humanElasticDataBuilder.isar41;
		this.isar42 = humanElasticDataBuilder.isar42;
		this.isar43 = humanElasticDataBuilder.isar43;
		this.isar44 = humanElasticDataBuilder.isar44;
		this.isar45 = humanElasticDataBuilder.isar45;
		this.isar46 = humanElasticDataBuilder.isar46;
		this.isar47 = humanElasticDataBuilder.isar47;
		this.isar48 = humanElasticDataBuilder.isar48;
		this.isar49 = humanElasticDataBuilder.isar49;
	}

	public static class HumanElasticDataBuilder {
		private String isar1;
		private String isar2;
		private String isar3;
		private String isar4;
		private String isar5;
		private String isar6;
		private String isar7;
		private String isar8;
		private String isar9;
		private String isar10;
		private Date isar11;
		private int isar12;
		private int isar13;
		private int isar14;
		private String isar15;
		private String isar16;
		private String isar17;
		private String isar18;
		private String isar19;
		private String isar20;
		private String isar21;
		private String isar22;
		private String isar23;
		private String isar24;
		private String isar25;
		private int isar26;
		private long isar27;
		private int isar28;
		private String isar29;
		private String isar30;
		private String isar31;
		private String isar32;
		private String isar33;
		private String isar34;
		private String isar35;
		private String isar36;
		private String isar37;
		private String isar38;
		private String isar39;
		private String isar40;
		private String isar41;
		private String isar42;
		private String isar43;
		private String isar44;
		private String isar45;
		private String isar46;
		private String isar47;
		private long isar48;
		private String isar49;

		public HumanElasticDataBuilder(final ServerRequestDataDto serverRequestDataDto) {
			this.isar1 = serverRequestDataDto.getIsar32();
			this.isar2 = serverRequestDataDto.getIsar1();
			this.isar3 = serverRequestDataDto.getIsar2();
			this.isar4 = serverRequestDataDto.getIsar3();
			this.isar5 = serverRequestDataDto.getIsar4();
			this.isar8 = serverRequestDataDto.getIsar5();
			this.isar9 = serverRequestDataDto.getIsar6();
			this.isar10 = serverRequestDataDto.getIsar7();
			this.isar11 = new Date(Long.parseLong(serverRequestDataDto.getIsar8()));
			this.isar15 = serverRequestDataDto.getIsar9();
			this.isar16 = serverRequestDataDto.getIsar10();
			this.isar22 = serverRequestDataDto.getIsar11();
			this.isar23 = serverRequestDataDto.getIsar12();
			this.isar24 = serverRequestDataDto.getIsar13();
			this.isar25 = serverRequestDataDto.getIsar14();
			this.isar29 = serverRequestDataDto.getIsar15();
			this.isar30 = serverRequestDataDto.getIsar16();
			this.isar31 = serverRequestDataDto.getIsar17();
			this.isar32 = serverRequestDataDto.getIsar18();
			this.isar33 = serverRequestDataDto.getIsar19();
			this.isar34 = serverRequestDataDto.getIsar20();
			this.isar35 = AJAX_REQUEST_IDENTIFIER.equalsIgnoreCase(serverRequestDataDto.getIsar21()) ? "true" : "false";
			this.isar36 = serverRequestDataDto.getIsar22();
			this.isar37 = serverRequestDataDto.getIsar23();
			this.isar38 = serverRequestDataDto.getIsar24();
			this.isar39 = serverRequestDataDto.getIsar25();
			this.isar40 = serverRequestDataDto.getIsar26();
			this.isar41 = serverRequestDataDto.getIsar27();
			this.isar42 = serverRequestDataDto.getIsar28();
			this.isar43 = serverRequestDataDto.getIsar29();
			this.isar44 = serverRequestDataDto.getIsar30();
			this.isar45 = serverRequestDataDto.getIsar31();
			this.isar46 = serverRequestDataDto.getIsar33();
			this.isar47 = serverRequestDataDto.getIsar34();
			this.isar49 = serverRequestDataDto.getIsar36();
		}

		public HumanElasticDataBuilder pageDetails(final UriDetailsDto uriDetailsDto) {
			this.isar6 = uriDetailsDto.getPath();
			this.isar7 = uriDetailsDto.getFilename();
			return this;
		}

		public HumanElasticDataBuilder minute(final int minute) {
			this.isar12 = minute;
			return this;
		}

		public HumanElasticDataBuilder hour(final int hour) {
			this.isar13 = hour;
			return this;
		}

		public HumanElasticDataBuilder dayOfWeek(final int dayOfWeek) {
			this.isar14 = dayOfWeek;
			return this;
		}

		public HumanElasticDataBuilder deviceDetails(final DeviceInformationDto deviceInfoDto) {
			String useragentFamily = deviceInfoDto.getUserAgentFamily();
			String useragentVersion = deviceInfoDto.getUserAgentVersion();
			this.isar17 = StringUtil.getNotNullValue(deviceInfoDto.getUserAgentType());
			this.isar18 = new StringBuilder(useragentFamily).append(" - ").append(useragentVersion).toString();
			this.isar19 = StringUtil.getNotNullValue(deviceInfoDto.getOsFamily());
			this.isar20 = StringUtil.getNotNullValue(deviceInfoDto.getOsName());
			this.isar21 = StringUtil.getNotNullValue(deviceInfoDto.getDeviceCategory());
			return this;
		}

		public HumanElasticDataBuilder numDaysActive(final int numDaysActive) {
			this.isar26 = numDaysActive;
			return this;
		}

		public HumanElasticDataBuilder sessionDuration(final long sessionDuration) {
			this.isar27 = sessionDuration;
			return this;
		}

		public HumanElasticDataBuilder pageSequence(final int pageSequence) {
			this.isar28 = pageSequence;
			return this;
		}

		public HumanElasticDataBuilder processingTimeDifference(final long timeDifference) {
			this.isar48 = timeDifference;
			return this;
		}

		public HumanElasticDataDto build() {
			return new HumanElasticDataDto(this);
		}
	}
}
