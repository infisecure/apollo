package com.apollo.storm.bolts;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.KafkaTopicEnum;
import com.apollo.constants.RequestCategoryEnum;
import com.apollo.dto.DeviceInformationDto;
import com.apollo.dto.HumanElasticDataDto;
import com.apollo.dto.HumanElasticDataDto.HumanElasticDataBuilder;
import com.apollo.dto.ServerRequestDataDto;
import com.apollo.dto.UriDetailsDto;
import com.apollo.kafka.ApolloProducer;
import com.apollo.util.CacheHandlerUtil;
import com.apollo.util.DateUtil;
import com.apollo.util.StringUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ReservedIPComponent extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ReservedIPComponent.class);

	private ObjectMapper mapper;
	private transient OutputCollector outputCollector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		LOGGER.info("Preparing Reserved IP Component Task Bolt.");
		int totalTasks = context.getComponentTasks(RequestCategoryEnum.RESERVED_IP.getRequestCategory()).size();
		LOGGER.debug("Total Task, {}", totalTasks);
		this.outputCollector = collector;
		this.mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

	}

	@Override
	public void execute(final Tuple tuple) {
		/** get input data from the tuple */
		String inputData = tuple.getString(0);

		/** check if the input data is not null or empty string */
		if (StringUtil.isNotEmpty(inputData)) {
			LOGGER.debug("Processing Reserved IP API data, {}", inputData);
			ServerRequestDataDto requestDataDto = null;
			try {
				/** convert json string to request object */
				requestDataDto = mapper.readValue(inputData, ServerRequestDataDto.class);

				/** get path and page name from the requested url */
				String requestedUrl = StringUtil.getNotNullValue(requestDataDto.getIsar4()).trim();
				UriDetailsDto uriDetailsDto = CacheHandlerUtil.processUrl(requestedUrl);

				/** get device details details from the user-agent */
				String userAgent = requestDataDto.getIsar10();
				DeviceInformationDto deviceInfoDto = CacheHandlerUtil.getUseragentDetails(userAgent);

				/**
				 * convert the time stamp to date and get minute, hour and day of week
				 */
				long requestTime = Long.parseLong(requestDataDto.getIsar8());
				Date apiHitTime = new Date(requestTime);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(apiHitTime);
				int minute = calendar.get(Calendar.MINUTE);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

				String pageSequenceCookie = StringUtil.getNotNullValue(requestDataDto.getIsar14().trim());
				String[] pageSequenceCookieArr = pageSequenceCookie.split("\\.");
				int pageNumber = StringUtil.isNotEmpty(pageSequenceCookieArr[2])
						? Integer.parseInt(pageSequenceCookieArr[2])
						: 0;

				Date firstSeen = null;
				int daysDifference = 0;
				long firstSeenTimestamp = 0;
				String[] identifierCookie = requestDataDto.getIsar12().trim().split("\\.");
				if (identifierCookie.length == 3) {
					firstSeenTimestamp = Long.parseLong(identifierCookie[identifierCookie.length - 1]);
					firstSeen = new Date(firstSeenTimestamp);
					daysDifference = DateUtil.getDaysDiff(apiHitTime, firstSeen);
				}

				long sessionDuration = DateUtil.getSessionDuration(requestTime, pageSequenceCookieArr[1]);
				long processingTime = Long.parseLong(requestDataDto.getIsar35()) - requestTime;

				HumanElasticDataDto humanElasticDataDto = new HumanElasticDataBuilder(requestDataDto)
						.pageDetails(uriDetailsDto).deviceDetails(deviceInfoDto).minute(minute).hour(hour)
						.dayOfWeek(dayOfWeek).numDaysActive(daysDifference).sessionDuration(sessionDuration)
						.pageSequence(pageNumber).processingTimeDifference(processingTime).build();

				ApolloProducer.publishMessage(KafkaTopicEnum.HUMAN.getKafkaTopic(),
						mapper.writeValueAsString(humanElasticDataDto));

				outputCollector.ack(tuple);
			} catch (IOException | ArrayIndexOutOfBoundsException e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception while parsing input data, {}", e);
			} catch (Exception e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception, {}", e);
			}
		} else {
			outputCollector.ack(tuple);
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// Auto-generated method stub
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public OutputCollector getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
	}

}
