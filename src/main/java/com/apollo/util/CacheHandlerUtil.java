package com.apollo.util;

import org.apache.log4j.Logger;

import com.apollo.dto.DeviceInformationDto;
import com.apollo.dto.DeviceInformationDto.DeviceInformationBuilder;
import com.apollo.dto.IPApiRespDto;
import com.apollo.dto.UriDetailsDto;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

public class CacheHandlerUtil {

	public static final Logger LOGGER = Logger.getLogger(CacheHandlerUtil.class);

	private static CacheManager cacheManager;
	private static Cache urlCache;
	private static Cache ipApiCache;
	private static Cache uaCache;

	private CacheHandlerUtil() {
	}

	public static CacheManager getCacheManager() {
		if (cacheManager == null)
			cacheManager = CacheManager.newInstance();
		return cacheManager;
	}

	public static Cache getUrlCache() {
		if (urlCache == null)
			urlCache = getCacheManager().getCache("UrlCache");
		return urlCache;
	}

	public static Cache getIpCache() {
		if (ipApiCache == null)
			ipApiCache = getCacheManager().getCache("IpCache");
		return ipApiCache;
	}

	public static Cache getUaCache() {
		if (uaCache == null)
			uaCache = getCacheManager().getCache("UaCache");
		return uaCache;
	}

	public static UriDetailsDto processUrl(String url) {
		if (StringUtil.isEmpty(url)) {
			LOGGER.error("URL to be processed cannot be null or empty!");
			return new UriDetailsDto("/", "/");
		}

		if (getUrlCache().isKeyInCache(url)) {
			Element element = getUrlCache().get(url);
			return (UriDetailsDto) element.getObjectValue();
		}

		UriDetailsDto uriDetailsDto = URIParserUtil.processUrl(url);
		getUrlCache().put(new Element(url, uriDetailsDto));
		return uriDetailsDto;
	}

	public static IPApiRespDto getIpDetails(String ipAddress) {
		if (StringUtil.isEmpty(ipAddress) || IPUtil.isReservedAddress(ipAddress)) {
			LOGGER.error("IP to be processed cannot be null, empty or reserved ip!");
			return new IPApiRespDto();
		}

		if (getIpCache().isKeyInCache(ipAddress)) {
			Element element = getIpCache().get(ipAddress);
			return (IPApiRespDto) element.getObjectValue();
		}

		IPApiRespDto ipApiRespDto = IPUtil.getIpApiDetail(ipAddress);
		getIpCache().put(new Element(ipAddress, ipApiRespDto));
		return ipApiRespDto;
	}

	public static DeviceInformationDto getUseragentDetails(String useragent) {
		if (StringUtil.isEmpty(useragent)) {
			LOGGER.error("Useragnet to be processed cannot be null or empty!");
			return new DeviceInformationBuilder(useragent, "", "", "").build();
		}

		if (getUaCache().isKeyInCache(useragent)) {
			Element element = getUaCache().get(useragent);
			return (DeviceInformationDto) element.getObjectValue();
		}

		UserAgentStringParser parser = UADetectorServiceFactory.getOnlineUpdatingParser();
		ReadableUserAgent agent = parser.parse(useragent);
		DeviceInformationDto deviceInfoDto = new DeviceInformationBuilder(agent.getName(), agent.getTypeName(),
				agent.getFamily().getName(), agent.getVersionNumber().toVersionString())
						.osDetails(agent.getOperatingSystem().getName(), agent.getOperatingSystem().getFamilyName(),
								agent.getOperatingSystem().getVersionNumber().toVersionString())
						.deviceCategory(agent.getDeviceCategory().getName()).build();
		getUaCache().put(new Element(useragent, deviceInfoDto));
		return deviceInfoDto;
	}

	public static void shutDownCacheManager() {
		if (cacheManager != null)
			cacheManager.shutdown();
	}
}
