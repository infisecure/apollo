package com.apollo.constants;

public enum ErrorCodeEnum {

	INVALID_ARGUMENT("ISCEX_6001"), INVALID_FILE("ISCEX_6002"), INVALID_ELASTIC_SEARCH_NODE("ISCEX_5051"),
	UNKNOWN_ELASTIC_SEARCH_HOST("ISCEX_5052");

	private String code;

	private ErrorCodeEnum(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return code;
	}

	public String getCode() {
		return code;
	}
}
