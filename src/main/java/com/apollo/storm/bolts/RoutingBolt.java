package com.apollo.storm.bolts;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.apollo.constants.RequestCategoryEnum;
import com.apollo.dto.ServerRequestDataDto;
import com.apollo.storm.jedis.JedisConnectionPool;
import com.apollo.util.StringUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RoutingBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(RoutingBolt.class);

	private ObjectMapper mapper;
	/**
	 * Create instance for OutputCollector which collects and emits tuples to
	 * produce output
	 */
	private transient OutputCollector outputCollector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map map, TopologyContext topologyContext,
			OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
		this.mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
	}

	@Override
	public void execute(final Tuple tuple) {
		String requestData = tuple.getString(0);
		ServerRequestDataDto requestDataDto = null;
		if (StringUtil.isNotEmpty(requestData)) {
			try {
				requestDataDto = mapper.readValue(requestData, ServerRequestDataDto.class);
				switch (requestDataDto.getIsar31()) {
				case "RESERVED_IP":
					outputCollector.emit(RequestCategoryEnum.RESERVED_IP.getRequestCategory(), new Values(requestData));
					break;

				case "EMPTY_IP":
					outputCollector.emit(RequestCategoryEnum.EMPTY_IP.getRequestCategory(), new Values(requestData));
					break;

				case "HUMAN":
					outputCollector.emit(RequestCategoryEnum.HUMAN.getRequestCategory(), new Values(requestData));
					break;

				case "CRAWLER":
					outputCollector.emit(RequestCategoryEnum.CRAWLER.getRequestCategory(), new Values(requestData));
					break;

				case "AFFIRM_CRAWLER":
					outputCollector.emit(RequestCategoryEnum.AFFIRM_CRAWLER.getRequestCategory(),
							new Values(requestData));
					break;

				case "BLACKLISTED":
					outputCollector.emit(RequestCategoryEnum.BLACKLISTED.getRequestCategory(), new Values(requestData));
					break;

				case "PROXY_IP":
					outputCollector.emit(RequestCategoryEnum.PROXY_IP.getRequestCategory(), new Values(requestData));
					break;

				case "TOR_EXIT_NODE":
					outputCollector.emit(RequestCategoryEnum.TOR_EXIT_NODES.getRequestCategory(),
							new Values(requestData));
					break;

				case "DATACENTER_AND_WEB_HOSTING":
					outputCollector.emit(RequestCategoryEnum.DATACENTER_IP.getRequestCategory(),
							new Values(requestData));
					break;

				case "FILTERED_REQUEST_ALLOWED":
					outputCollector.emit(RequestCategoryEnum.FILTERED_REQUEST_ALLOWED.getRequestCategory(),
							new Values(requestData));
					break;

				default:
					break;
				}
			} catch (IOException e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception while mapping to object, {}", e);
			}
			outputCollector.ack(tuple);
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.RESERVED_IP.getRequestCategory(),
				new Fields(RequestCategoryEnum.RESERVED_IP.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.EMPTY_IP.getRequestCategory(),
				new Fields(RequestCategoryEnum.EMPTY_IP.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.HUMAN.getRequestCategory(),
				new Fields(RequestCategoryEnum.HUMAN.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.CRAWLER.getRequestCategory(),
				new Fields(RequestCategoryEnum.CRAWLER.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.AFFIRM_CRAWLER.getRequestCategory(),
				new Fields(RequestCategoryEnum.AFFIRM_CRAWLER.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.BLACKLISTED.getRequestCategory(),
				new Fields(RequestCategoryEnum.BLACKLISTED.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.PROXY_IP.getRequestCategory(),
				new Fields(RequestCategoryEnum.PROXY_IP.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.TOR_EXIT_NODES.getRequestCategory(),
				new Fields(RequestCategoryEnum.TOR_EXIT_NODES.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.DATACENTER_IP.getRequestCategory(),
				new Fields(RequestCategoryEnum.DATACENTER_IP.getRequestCategory()));
		outputFieldsDeclarer.declareStream(RequestCategoryEnum.FILTERED_REQUEST_ALLOWED.getRequestCategory(),
				new Fields(RequestCategoryEnum.FILTERED_REQUEST_ALLOWED.getRequestCategory()));
	}

	@Override
	public void cleanup() {
		JedisConnectionPool.destroyPool();
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public OutputCollector getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
	}
}
