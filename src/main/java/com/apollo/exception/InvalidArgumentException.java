package com.apollo.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import com.apollo.constants.ErrorCodeEnum;
import com.apollo.util.StringUtil;

public class InvalidArgumentException extends Exception {

	private static final long serialVersionUID = 1L;

	private final ErrorCodeEnum code;

	public InvalidArgumentException(ErrorCodeEnum code) {
		super();
		this.code = code;
	}

	public InvalidArgumentException(String message, ErrorCodeEnum code) {
		super(message);
		this.code = code;
	}

	public InvalidArgumentException(String message, ErrorCodeEnum code, String... searchParamsMap) {
		super(InvalidArgumentException.generateMessage(message,
				toMap(String.class, String.class, (Object[]) searchParamsMap)));
		this.code = code;
	}

	private static String generateMessage(String message, Map<String, String> searchParams) {
		return StringUtil.capitalizeString(message) + ". Params - " + searchParams;
	}

	private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
		if (entries.length % 2 == 1)
			throw new IllegalArgumentException("Invalid entries!");
		return IntStream.range(0, entries.length / 2).map(i -> i * 2).collect(HashMap::new,
				(m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])), Map::putAll);
	}

	public ErrorCodeEnum getCode() {
		return code;
	}
}
