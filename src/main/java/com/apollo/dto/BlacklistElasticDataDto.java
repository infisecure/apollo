package com.apollo.dto;

import java.io.Serializable;
import java.util.Date;

import com.apollo.util.StringUtil;

import lombok.Data;

@Data
public class BlacklistElasticDataDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String AJAX_REQUEST_IDENTIFIER = "XMLHttpRequest";

	private BlacklistElasticDataDto() {
		super();
	}

	private String isbl1;
	private String isbl2;
	private String isbl3;
	private String isbl4;
	private String isbl5;
	private String isbl6;
	private String isbl7;
	private String isbl8;
	private String isbl9;
	private String isbl10;
	private Date isbl11;
	private int isbl12;
	private int isbl13;
	private int isbl14;
	private String isbl15;
	private String isbl16;
	private String isbl17;
	private String isbl18;
	private String isbl19;
	private String isbl20;
	private String isbl21;
	private String isbl22;
	private String isbl23;
	private String isbl24;
	private String isbl25;
	private String isbl26;
	private String isbl27;
	private String isbl28;
	private String isbl29;
	private String isbl30;
	private String isbl31;
	private String isbl32;
	private String isbl33;
	private int isbl34;
	private long isbl35;
	private int isbl36;
	private String isbl37;
	private String isbl38;
	private String isbl39;
	private String isbl40;
	private String isbl41;
	private String isbl42;
	private String isbl43;
	private String isbl44;
	private String isbl45;
	private String isbl46;
	private String isbl47;
	private String isbl48;
	private String isbl49;
	private String isbl50;
	private String isbl51;
	private String isbl52;
	private String isbl53;
	private String isbl54;
	private String isbl55;
	private long isbl56;
	private String isbl57;
	private String isbl58;
	private String isbl59;
	private String isbl60;
	private String isbl61;
	private String isbl62;

	private BlacklistElasticDataDto(BlacklistElasticDataBuilder blacklistElasticDataBuilder) {
		super();
		this.isbl1 = blacklistElasticDataBuilder.isbl1;
		this.isbl2 = blacklistElasticDataBuilder.isbl2;
		this.isbl3 = blacklistElasticDataBuilder.isbl3;
		this.isbl4 = blacklistElasticDataBuilder.isbl4;
		this.isbl5 = blacklistElasticDataBuilder.isbl5;
		this.isbl6 = blacklistElasticDataBuilder.isbl6;
		this.isbl7 = blacklistElasticDataBuilder.isbl7;
		this.isbl8 = blacklistElasticDataBuilder.isbl8;
		this.isbl9 = blacklistElasticDataBuilder.isbl9;
		this.isbl10 = blacklistElasticDataBuilder.isbl10;
		this.isbl11 = blacklistElasticDataBuilder.isbl11;
		this.isbl12 = blacklistElasticDataBuilder.isbl12;
		this.isbl13 = blacklistElasticDataBuilder.isbl13;
		this.isbl14 = blacklistElasticDataBuilder.isbl14;
		this.isbl15 = blacklistElasticDataBuilder.isbl15;
		this.isbl16 = blacklistElasticDataBuilder.isbl16;
		this.isbl17 = blacklistElasticDataBuilder.isbl17;
		this.isbl18 = blacklistElasticDataBuilder.isbl18;
		this.isbl19 = blacklistElasticDataBuilder.isbl19;
		this.isbl20 = blacklistElasticDataBuilder.isbl20;
		this.isbl21 = blacklistElasticDataBuilder.isbl21;
		this.isbl22 = blacklistElasticDataBuilder.isbl22;
		this.isbl23 = blacklistElasticDataBuilder.isbl23;
		this.isbl24 = blacklistElasticDataBuilder.isbl24;
		this.isbl25 = blacklistElasticDataBuilder.isbl25;
		this.isbl26 = blacklistElasticDataBuilder.isbl26;
		this.isbl27 = blacklistElasticDataBuilder.isbl27;
		this.isbl28 = blacklistElasticDataBuilder.isbl28;
		this.isbl29 = blacklistElasticDataBuilder.isbl29;
		this.isbl30 = blacklistElasticDataBuilder.isbl30;
		this.isbl31 = blacklistElasticDataBuilder.isbl31;
		this.isbl32 = blacklistElasticDataBuilder.isbl32;
		this.isbl33 = blacklistElasticDataBuilder.isbl33;
		this.isbl34 = blacklistElasticDataBuilder.isbl34;
		this.isbl35 = blacklistElasticDataBuilder.isbl35;
		this.isbl36 = blacklistElasticDataBuilder.isbl36;
		this.isbl37 = blacklistElasticDataBuilder.isbl37;
		this.isbl38 = blacklistElasticDataBuilder.isbl38;
		this.isbl39 = blacklistElasticDataBuilder.isbl39;
		this.isbl40 = blacklistElasticDataBuilder.isbl40;
		this.isbl41 = blacklistElasticDataBuilder.isbl41;
		this.isbl42 = blacklistElasticDataBuilder.isbl42;
		this.isbl43 = blacklistElasticDataBuilder.isbl43;
		this.isbl44 = blacklistElasticDataBuilder.isbl44;
		this.isbl45 = blacklistElasticDataBuilder.isbl45;
		this.isbl46 = blacklistElasticDataBuilder.isbl46;
		this.isbl47 = blacklistElasticDataBuilder.isbl47;
		this.isbl48 = blacklistElasticDataBuilder.isbl48;
		this.isbl49 = blacklistElasticDataBuilder.isbl49;
		this.isbl50 = blacklistElasticDataBuilder.isbl50;
		this.isbl51 = blacklistElasticDataBuilder.isbl51;
		this.isbl52 = blacklistElasticDataBuilder.isbl52;
		this.isbl53 = blacklistElasticDataBuilder.isbl53;
		this.isbl54 = blacklistElasticDataBuilder.isbl54;
		this.isbl55 = blacklistElasticDataBuilder.isbl55;
		this.isbl56 = blacklistElasticDataBuilder.isbl56;
		this.isbl57 = blacklistElasticDataBuilder.isbl57;
		this.isbl58 = blacklistElasticDataBuilder.isbl58;
		this.isbl59 = blacklistElasticDataBuilder.isbl59;
		this.isbl60 = blacklistElasticDataBuilder.isbl60;
		this.isbl61 = blacklistElasticDataBuilder.isbl61;
		this.isbl62 = blacklistElasticDataBuilder.isbl62;
	}

	public static class BlacklistElasticDataBuilder {
		private String isbl1;
		private String isbl2;
		private String isbl3;
		private String isbl4;
		private String isbl5;
		private String isbl6;
		private String isbl7;
		private String isbl8;
		private String isbl9;
		private String isbl10;
		private Date isbl11;
		private int isbl12;
		private int isbl13;
		private int isbl14;
		private String isbl15;
		private String isbl16;
		private String isbl17;
		private String isbl18;
		private String isbl19;
		private String isbl20;
		private String isbl21;
		private String isbl22;
		private String isbl23;
		private String isbl24;
		private String isbl25;
		private String isbl26;
		private String isbl27;
		private String isbl28;
		private String isbl29;
		private String isbl30;
		private String isbl31;
		private String isbl32;
		private String isbl33;
		private int isbl34;
		private long isbl35;
		private int isbl36;
		private String isbl37;
		private String isbl38;
		private String isbl39;
		private String isbl40;
		private String isbl41;
		private String isbl42;
		private String isbl43;
		private String isbl44;
		private String isbl45;
		private String isbl46;
		private String isbl47;
		private String isbl48;
		private String isbl49;
		private String isbl50;
		private String isbl51;
		private String isbl52;
		private String isbl53;
		private String isbl54;
		private String isbl55;
		private long isbl56;
		private String isbl57;
		private String isbl58;
		private String isbl59;
		private String isbl60;
		private String isbl61;
		private String isbl62;

		public BlacklistElasticDataBuilder(final ServerRequestDataDto serverRequestDataDto) {
			this.isbl1 = serverRequestDataDto.getIsar32();
			this.isbl2 = serverRequestDataDto.getIsar1();
			this.isbl3 = serverRequestDataDto.getIsar2();
			this.isbl4 = serverRequestDataDto.getIsar3();
			this.isbl5 = serverRequestDataDto.getIsar4();
			this.isbl8 = serverRequestDataDto.getIsar5();
			this.isbl9 = serverRequestDataDto.getIsar6();
			this.isbl10 = serverRequestDataDto.getIsar7();
			this.isbl11 = new Date(Long.parseLong(serverRequestDataDto.getIsar8()));
			this.isbl15 = serverRequestDataDto.getIsar9();
			this.isbl24 = serverRequestDataDto.getIsar10();
			this.isbl30 = serverRequestDataDto.getIsar11();
			this.isbl31 = serverRequestDataDto.getIsar12();
			this.isbl32 = serverRequestDataDto.getIsar13();
			this.isbl33 = serverRequestDataDto.getIsar14();
			this.isbl37 = serverRequestDataDto.getIsar15();
			this.isbl38 = serverRequestDataDto.getIsar16();
			this.isbl39 = serverRequestDataDto.getIsar17();
			this.isbl40 = serverRequestDataDto.getIsar18();
			this.isbl41 = serverRequestDataDto.getIsar19();
			this.isbl42 = serverRequestDataDto.getIsar20();
			this.isbl43 = AJAX_REQUEST_IDENTIFIER.equalsIgnoreCase(serverRequestDataDto.getIsar21()) ? "true" : "false";
			this.isbl44 = serverRequestDataDto.getIsar22();
			this.isbl45 = serverRequestDataDto.getIsar23();
			this.isbl46 = serverRequestDataDto.getIsar24();
			this.isbl47 = serverRequestDataDto.getIsar25();
			this.isbl48 = serverRequestDataDto.getIsar26();
			this.isbl49 = serverRequestDataDto.getIsar27();
			this.isbl50 = serverRequestDataDto.getIsar28();
			this.isbl51 = serverRequestDataDto.getIsar29();
			this.isbl52 = serverRequestDataDto.getIsar30();
			this.isbl53 = serverRequestDataDto.getIsar31();
			this.isbl54 = serverRequestDataDto.getIsar33();
			this.isbl55 = serverRequestDataDto.getIsar34();
			this.isbl58 = serverRequestDataDto.getIsar36();
			this.isbl59 = serverRequestDataDto.getIsar37();
			this.isbl60 = serverRequestDataDto.getIsar38();
			this.isbl61 = serverRequestDataDto.getIsar39();
			this.isbl62 = serverRequestDataDto.getIsar40();
		}

		public BlacklistElasticDataBuilder pageDetails(final UriDetailsDto uriDetailsDto) {
			this.isbl6 = uriDetailsDto.getPath();
			this.isbl7 = uriDetailsDto.getFilename();
			return this;
		}

		public BlacklistElasticDataBuilder minute(final int minute) {
			this.isbl12 = minute;
			return this;
		}

		public BlacklistElasticDataBuilder hour(final int hour) {
			this.isbl13 = hour;
			return this;
		}

		public BlacklistElasticDataBuilder dayOfWeek(final int dayOfWeek) {
			this.isbl14 = dayOfWeek;
			return this;
		}

		public BlacklistElasticDataBuilder reverseDns(final String reverseDns) {
			this.isbl22 = reverseDns;
			return this;
		}

		public BlacklistElasticDataBuilder ipDetails(final IPApiRespDto ipApiRespDto) {
			this.isbl16 = ipApiRespDto.getRegionName();
			this.isbl17 = ipApiRespDto.getRegion();
			this.isbl18 = ipApiRespDto.getCity();
			this.isbl19 = ipApiRespDto.getCountry();
			this.isbl20 = ipApiRespDto.getCountryCode();
			this.isbl21 = ipApiRespDto.getIsp();
			this.isbl23 = ipApiRespDto.getAs();
			return this;
		}

		public BlacklistElasticDataBuilder deviceDetails(final DeviceInformationDto deviceInfoDto) {
			String useragentFamily = deviceInfoDto.getUserAgentFamily();
			String useragentVersion = deviceInfoDto.getUserAgentVersion();
			this.isbl25 = StringUtil.getNotNullValue(deviceInfoDto.getUserAgentType());
			this.isbl26 = new StringBuilder(useragentFamily).append(" - ").append(useragentVersion).toString();
			this.isbl27 = StringUtil.getNotNullValue(deviceInfoDto.getOsFamily());
			this.isbl28 = StringUtil.getNotNullValue(deviceInfoDto.getOsName());
			this.isbl29 = StringUtil.getNotNullValue(deviceInfoDto.getDeviceCategory());
			return this;
		}

		public BlacklistElasticDataBuilder numDaysActive(final int numDaysActive) {
			this.isbl34 = numDaysActive;
			return this;
		}

		public BlacklistElasticDataBuilder sessionDuration(final long sessionDuration) {
			this.isbl35 = sessionDuration;
			return this;
		}

		public BlacklistElasticDataBuilder pageSequence(final int pageSequence) {
			this.isbl36 = pageSequence;
			return this;
		}

		public BlacklistElasticDataBuilder processingTimeDifference(final long timeDifference) {
			this.isbl56 = timeDifference;
			return this;
		}

		public BlacklistElasticDataBuilder usageType(final String usageType) {
			this.isbl57 = usageType;
			return this;
		}

		public BlacklistElasticDataDto build() {
			return new BlacklistElasticDataDto(this);
		}
	}
}
