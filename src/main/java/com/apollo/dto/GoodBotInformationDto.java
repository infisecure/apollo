package com.apollo.dto;

import java.io.Serializable;

import com.apollo.util.StringUtil;

import lombok.Data;

@Data
public class GoodBotInformationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private GoodBotInformationDto() {
		super();
	}

	private String domainCode;
	private String ipAddress;
	private String crawlerFamily;
	private String crawlerName;
	private String crawlerCategory;

	private GoodBotInformationDto(GoodBotInformationBuilder goodBotInformationBuilder) {
		super();
		this.domainCode = goodBotInformationBuilder.domainCode;
		this.ipAddress = goodBotInformationBuilder.ipAddress;
		this.crawlerFamily = goodBotInformationBuilder.crawlerFamily;
		this.crawlerName = goodBotInformationBuilder.crawlerName;
		this.crawlerCategory = goodBotInformationBuilder.crawlerCategory;
	}

	public static class GoodBotInformationBuilder {
		private String domainCode;
		private String ipAddress;
		private String crawlerFamily;
		private String crawlerName;
		private String crawlerCategory;

		public GoodBotInformationBuilder(String domainCode, String ipAddress) {
			this.domainCode = domainCode;
			this.ipAddress = ipAddress;
		}

		public GoodBotInformationBuilder crawlerFamily(String crawlerFamily) {
			if (StringUtil.isNotEmpty(crawlerFamily))
				this.crawlerFamily = crawlerFamily;
			return this;
		}

		public GoodBotInformationBuilder crawlerName(String crawlerName) {
			if (StringUtil.isNotEmpty(crawlerName))
				this.crawlerName = crawlerName;
			return this;
		}

		public GoodBotInformationBuilder crawlerCategory(String crawlerCategory) {
			if (StringUtil.isNotEmpty(crawlerCategory))
				this.crawlerCategory = crawlerCategory;
			return this;
		}

		public GoodBotInformationDto build() {
			return new GoodBotInformationDto(this);
		}
	}
}
