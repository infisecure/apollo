package com.apollo.constants;

public enum KafkaTopicEnum {

	HUMAN("human"), HUMAN_IP_SUMMARY("human_ip_summary"), BAD_BOTS("bad_bots"), GOOD_BOTS("good_bots"),
	ANALYZE_REQUEST("analyze_request"), BLACKLIST("blacklist"), GOOD_BOTS_SUMMARY("good_bots_summary"),
	CREATE_CRAWLER("create_crawler");

	private String kafkaTopic;

	private KafkaTopicEnum(String kafkaTopic) {
		this.kafkaTopic = kafkaTopic;
	}

	public String getKafkaTopic() {
		return kafkaTopic;
	}

	@Override
	public String toString() {
		return kafkaTopic;
	}

	public String getName() {
		return this.name();
	}

}
