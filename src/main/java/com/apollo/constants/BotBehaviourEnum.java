package com.apollo.constants;

public enum BotBehaviourEnum {

	SCRAPER_BOT("Scraper Bot"), SPAMMER_BOT("Spammer Bot"), SCALPER_BOT("Scalper Bot"),
	SERVER_SLOWDOWN("Server Slowdown"), WEBSITE_EXTRACTOR("Website Extractor/Downloader"),
	HACKER("Hacker (SQL/ShellShock Attack)"), CLICK_BOT("Click Bot"), BRUTE_FORCE_ATTACK("Brute Force Attack"),
	VULNERABILITY_SCANNING("Vulnerability Scanning"), BAD_WEB_HOST("Bad Web Host"),
	UNCATEGORIZED_BOT("Uncategorized Bot");

	private String botBehaviour;

	private BotBehaviourEnum(String botBehaviour) {
		this.botBehaviour = botBehaviour;
	}

	public String getBotBehaviour() {
		return botBehaviour;
	}

	@Override
	public String toString() {
		return this.botBehaviour;
	}

	public String getName() {
		return this.name();
	}
}
