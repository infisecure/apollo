package com.apollo.constants;

public enum IPVersionEnum {

	IPV4("IPv4"), IPV6("IPv6");

	private String ipVersion;

	private IPVersionEnum(final String ipVersion) {
		this.ipVersion = ipVersion;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	@Override
	public String toString() {
		return ipVersion;
	}

	public String getName() {
		return this.name();
	}

}
