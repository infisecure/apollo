package com.apollo.util;

import java.util.Map;

import com.apollo.constants.HttpRequestMethodEnum;
import com.apollo.dto.DeviceInformationDto;
import com.apollo.dto.HumanElasticDataDto;

public class AclUtil {

	private static final String CONTINUE = "CONTINUE";

	private static final String ANALYZE_IP = "ANALYZE-IP-ADDRESS";
	private static final String EMPTY_REFERER = "BLACKLIST-EMPTY-REFERER";
	private static final String SAME_REFERER_REQUEST = "BLACKLIST-SAME-REQ-REF";
	private static final String BAD_REQUEST_METHOD = "BLACKLIST-BAD-REQUEST-METHOD";
	private static final String SQL_INJECTION = "BLACKLIST-SQL-INJECTION";
	private static final String TAMPERED_COOKIE = "TAMPERED-COOKIE";
	private static final String NON_STANDARD_UA = "NON-STANDARD-UA";

	private static final String ANALYSIS_THRESHOLD = "analysis_init_threshold";
	private static final String SAME_REFERER_REQUEST_THRESHOLD = "same_req_ref_page_threshold";

	private AclUtil() {
		super();
	}

	public static String analyzeHumanRequest(Map<String, String> aclRateLimitingMap,
			HumanElasticDataDto humanElasticDataDto, DeviceInformationDto deviceInfoDto, long totalRequest,
			long totalRequestSameRefReq, int pageNumber, int incrementedValue) {

		String refererUrl = StringUtil.getNotNullValue(humanElasticDataDto.getIsar4().trim());
		String requestedUrl = StringUtil.getNotNullValue(humanElasticDataDto.getIsar5().trim());
		String requestMethod = StringUtil.getNotNullValue(humanElasticDataDto.getIsar9().trim());
		String headersList = StringUtil.getNotNullValue(humanElasticDataDto.getIsar31().trim());
		String userAgent = StringUtil.getNotNullValue(humanElasticDataDto.getIsar16().trim());

		if (HttpRequestMethodEnum.TRACE.getRequestMethod().equals(requestMethod))
			return BAD_REQUEST_METHOD;

		if ("Other".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Library".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Offline Browser".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Robot".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "UNKNOWN".equalsIgnoreCase(deviceInfoDto.getUserAgentType())
				|| "Useragent Anonymizer".equalsIgnoreCase(deviceInfoDto.getUserAgentType()))
			return NON_STANDARD_UA;

		if (totalRequestSameRefReq >= Integer.parseInt(aclRateLimitingMap.get(SAME_REFERER_REQUEST_THRESHOLD)))
			return SAME_REFERER_REQUEST;

		if (StringUtil.isEmpty(headersList))
			return ANALYZE_IP;

		if (StringUtil.isEmpty(refererUrl) && requestedUrl.length() > 250 && totalRequest >= 5
				&& HttpRequestMethodEnum.POST.getRequestMethod().equalsIgnoreCase(requestMethod))
			return EMPTY_REFERER;

		if (StringUtil.isEmpty(refererUrl) && requestedUrl.length() > 150)
			return ANALYZE_IP;

		if ((incrementedValue / 7) != pageNumber - 1)
			return TAMPERED_COOKIE;

		if (!SQLInjectionFilter.isSqlInjectionSafe(userAgent) || !SQLInjectionFilter.isSqlInjectionSafe(requestedUrl))
			return SQL_INJECTION;

		if (totalRequest >= Integer.parseInt(aclRateLimitingMap.get(ANALYSIS_THRESHOLD)))
			return ANALYZE_IP;

		return CONTINUE;
	}
}
