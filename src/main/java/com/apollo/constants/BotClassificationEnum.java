package com.apollo.constants;

public enum BotClassificationEnum {

	FILTERED_IP("Filtered IP"), FILTERED_UA("Filtered UA"), ORGANIZATION_BLOCK("Organization Block"),
	BLOCKED_COUNTRY("Blocked Country"), REFERRER_BLOCK("Referrer Block"), FILTERED_CRAWLER("Filtered Crawler"),
	EMPTY_OR_BAD_UA("Empty/Known Bad User Agent"), DEVELOPER_OR_HACKING_TOOL("Developer/Hacking Tool"),
	GLOBAL_BAD_IP("Global Bad IP Address"), IMPROPER_HEADER("Improper Header Name/Value Pair"),
	AUTOMATED_BROWSER("Automated Browser/Webdriver"), NON_STANDARD_UA("Non Standard Useragent Build"),
	TYPOGRAPHIC_ERROR("Typographic Error"), SESSION_LENGTH_EXCEEDED("Session Length Exceeded"),
	PAGE_PER_MINUTE_EXCEEDED("Request Per Minute Exceeded"), PAGE_PER_SESSION_EXCEEDED("Request Per Session Exceeded"),
	HONEY_POT_ACCESS("Honeypot Access"), PROGRAMATIC_BROWSING("Programatic Browsing"),
	BAD_REQUEST_METHOD("Bad Http Request Method"), BAD_DEVICE("Bad Device"), TOR_EXIT_NODE("TOR Exit Node"),
	DATACENTER("Datacenter/Web Hosting"), IMPERSONATOR("Impersonator"), RESERVED_IP("Reserved IP");

	private String botClassification;

	private BotClassificationEnum(final String botClassification) {
		this.botClassification = botClassification;
	}

	public String getBotClassification() {
		return botClassification;
	}

	@Override
	public String toString() {
		return this.botClassification;
	}

	public String getName() {
		return this.name();
	}
}
