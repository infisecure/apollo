package com.apollo.kafka;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApolloCallback implements Callback {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApolloCallback.class);
	private final String message;

	public ApolloCallback(String message) {
		super();
		this.message = message;
	}

	@Override
	public void onCompletion(RecordMetadata metadata, Exception e) {
		if (e != null)
			LOGGER.error("Unable to publish the message to kafka, Message - {0}, Exception - {1}", message, e);
	}

	public String getMessage() {
		return message;
	}
}
