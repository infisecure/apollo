package com.apollo.storm;

import org.apache.storm.kafka.BrokerHosts;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.StringScheme;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.Constant;
import com.apollo.constants.ErrorCodeEnum;
import com.apollo.exception.InvalidArgumentException;
import com.apollo.util.StringUtil;

public class StormSpoutConfig {

	public static final Logger LOGGER = LoggerFactory.getLogger(StormSpoutConfig.class);

	private StormSpoutConfig() {
	}

	public static SpoutConfig initialize(final String zkHost, final String topic, final String zkRoot,
			final String clientId) throws InvalidArgumentException {
		if (StringUtil.isEmpty(zkHost) || StringUtil.isEmpty(topic) || StringUtil.isEmpty(zkRoot)
				|| StringUtil.isEmpty(clientId))
			throw new InvalidArgumentException("One or more specified arguments not valid",
					ErrorCodeEnum.INVALID_ARGUMENT, "Zookeeper Host", zkHost, "Kafka Topic", topic, "Zookeeper Root",
					zkRoot, "Spout Client ID", clientId);

		String zookeeperHost = zkHost;
		String kafkaTopic = topic;
		String zookeeperRoot = zkRoot;
		String spoutClientId = clientId;
		return initializeSpout(zookeeperHost, kafkaTopic, zookeeperRoot, spoutClientId);
	}

	private static SpoutConfig initializeSpout(String zookeeperHost, String kafkaTopic, String zookeeperRoot,
			String spoutClientId) {
		BrokerHosts brokerHosts = new ZkHosts(zookeeperHost);
		SpoutConfig spoutConfig = new SpoutConfig(brokerHosts, kafkaTopic, zookeeperRoot, spoutClientId);
		spoutConfig.bufferSizeBytes = Constant.SPOUT_BUFFER_SIZE;
		spoutConfig.fetchSizeBytes = Constant.SPOUT_FETCH_BYTE_SIZE;
		spoutConfig.minFetchByte = Constant.SPOUT_MIN_FETCH_BYTE_SIZE;
		spoutConfig.fetchMaxWait = Constant.SPOUT_FETCH_MAX_WAIT;
		spoutConfig.socketTimeoutMs = Constant.SPOUT_SOCKET_TIMEOUT_MS;
		spoutConfig.ignoreZkOffsets = false;
		spoutConfig.startOffsetTime = kafka.api.OffsetRequest.LatestTime();
		spoutConfig.maxOffsetBehind = Long.MIN_VALUE;
		spoutConfig.retryDelayMaxMs = -1;
		spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
		spoutConfig.useStartOffsetTimeIfOffsetOutOfRange = true;
		LOGGER.info("Successfully created spout config!");
		return spoutConfig;
	}
}