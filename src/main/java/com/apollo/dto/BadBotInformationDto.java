package com.apollo.dto;

import java.io.Serializable;

import com.apollo.util.StringUtil;

import lombok.Data;

@Data
public class BadBotInformationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private BadBotInformationDto() {
		super();
	}

	private String domainCode;
	private String serviceCode;
	private String ipAddress;
	private String ruleName;
	private String paramKey;
	private String paramValue;
	private String botCategory;
	private String botClass;
	private String botBehaviour;
	private String message;

	private BadBotInformationDto(BadBotInformationBuilder badBotInformationBuilder) {
		super();
		this.domainCode = badBotInformationBuilder.domainCode;
		this.serviceCode = badBotInformationBuilder.serviceCode;
		this.ipAddress = badBotInformationBuilder.ipAddress;
		this.ruleName = badBotInformationBuilder.ruleName;
		this.paramKey = badBotInformationBuilder.paramKey;
		this.paramValue = badBotInformationBuilder.paramValue;
		this.botCategory = badBotInformationBuilder.botCategory;
		this.botClass = badBotInformationBuilder.botClass;
		this.botBehaviour = badBotInformationBuilder.botBehaviour;
		this.message = badBotInformationBuilder.message;
	}

	public static class BadBotInformationBuilder {
		private String domainCode;
		private String serviceCode;
		private String ipAddress;
		private String ruleName;
		private String paramKey;
		private String paramValue;
		private String botCategory;
		private String botClass;
		private String botBehaviour;
		private String message;

		public BadBotInformationBuilder(String domainCode, String serviceCode, String ipAddress) {
			this.domainCode = domainCode;
			this.serviceCode = serviceCode;
			this.ipAddress = ipAddress;
		}

		public BadBotInformationBuilder ruleName(String ruleName) {
			if (StringUtil.isNotEmpty(ruleName))
				this.ruleName = ruleName;
			return this;
		}

		public BadBotInformationBuilder paramKey(String paramKey) {
			if (StringUtil.isNotEmpty(paramKey))
				this.paramKey = paramKey;
			return this;
		}

		public BadBotInformationBuilder paramValue(String paramValue) {
			if (StringUtil.isNotEmpty(paramValue))
				this.paramValue = paramValue;
			return this;
		}

		public BadBotInformationBuilder botCategory(String botCategory) {
			if (StringUtil.isNotEmpty(botCategory))
				this.botCategory = botCategory;
			return this;
		}

		public BadBotInformationBuilder botClass(String botClass) {
			if (StringUtil.isNotEmpty(botClass))
				this.botClass = botClass;
			return this;
		}

		public BadBotInformationBuilder botBehaviour(String botBehaviour) {
			if (StringUtil.isNotEmpty(botClass))
				this.botBehaviour = botBehaviour;
			return this;
		}

		public BadBotInformationBuilder message(String message) {
			if (StringUtil.isNotEmpty(message))
				this.message = message;
			return this;
		}

		public BadBotInformationDto build() {
			return new BadBotInformationDto(this);
		}
	}
}
