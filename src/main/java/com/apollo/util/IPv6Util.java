package com.apollo.util;

import java.math.BigInteger;

import com.apollo.constants.Constant;
import com.googlecode.ipv6.IPv6Address;
import com.googlecode.ipv6.IPv6Network;

public class IPv6Util {

	public static BigInteger toBigIntegerIpv6(String ipAddress) {
		if (ipAddress == null || ipAddress.isEmpty()) {
			throw new IllegalArgumentException("IP Address cannot be null or empty");
		}
		return IPv6Address.fromString(ipAddress).toBigInteger();
	}

	public static IPv6Address bigInteger2Ip(BigInteger ipAddress) {
		if (ipAddress.compareTo(Constant.MAX_IPV6_RANGE) > 1 || ipAddress.compareTo(BigInteger.valueOf(0)) < 0) {
			throw new IllegalArgumentException("Invalid IP BigInteger respresentation.");
		}
		return IPv6Address.fromBigInteger(ipAddress);
	}

	public static String[] getIpRangeFromCidr(String ipCidr) {
		final String[] result = new String[2];
		if (isValidIp6Cidr(ipCidr)) {
			IPv6Network strangeNetwork = IPv6Network.fromString(ipCidr);
			result[0] = strangeNetwork.getFirst().toString();
			result[1] = strangeNetwork.getLast().toString();
		}

		return result;
	}

	public static boolean isValidIp6Cidr(final String ip6Cidr) {
		try {
			IPv6Network.fromString(ip6Cidr);
		} catch (final IllegalArgumentException ex) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
//		String ipAddress = "2a03:b0c0:1:199::";
//		System.out.println(ip2BigInteger(ipAddress));
//		System.out.println(bigInteger2Ip(new BigInteger("55846737629440733048130861473808252928")));
		String[] ipv6Range = getIpRangeFromCidr("2a03:b0c0:0:1049::/64");
		System.out.println("First - " + ipv6Range[0]);
		System.out.println("Last - " + ipv6Range[1]);
		System.out.println(toBigIntegerIpv6(ipv6Range[1]));
		IPv6Address next = IPv6Address.fromString(ipv6Range[0]).add(15);
		System.out.println(toBigIntegerIpv6(next.toString()).floatValue());
		System.out.println(toBigIntegerIpv6(next.toString()).doubleValue());

//		System.out.println(ip2BigInteger(ipv6Range[1]));

	}
}
