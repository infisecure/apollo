/*******************************************************************************
 *    Copyright (c) 2015 LeapNext (P) Limited. All Rights Reserved.
 *    This file is part of CoalitionCard.
 *    Proprietary and confidential.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *   
 *    @version     1.0, 2015
 *    Contributors: 
 *    Ashish Kumar Singh
 *     
 *    Description: write me
 *    
 *    ChangeLog:
 *    joe@rev 232: Added flooEnum.
 *******************************************************************************/
package com.apollo.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class StringUtil {
	private static final String GMAIL_EMAIL_SUFFIX = "@gmail.com";
	private static final int GMAIL_EMAIL_SUFFIX_LENGTH = GMAIL_EMAIL_SUFFIX.length();
	public static final String EMPTY_STRING = "";

	public static String getRandom(int length) {
		String randomString = getRandom();
		return randomString.substring(randomString.length() - length);
	}

	public static String getRandomAlphaNumeric(int length) {
		String aplhaNumberic = getRandom().toLowerCase().replaceAll("[^\\da-z]", "");
		return aplhaNumberic.substring(aplhaNumberic.length() - length);
	}

	public static String getRandomNumeric(int length) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			builder.append((int) (new Random().nextInt() * 10));
		}
		return builder.toString();
	}

	public static String getRandom() {
		return UUID.randomUUID().toString();
	}

	public static boolean isEmpty(String str) {
		if (str == null)
			return true;
		return "".equals(str.trim());
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static String getAccessorNameForField(String name) {
		return new StringBuffer("get").append(name.substring(0, 1).toUpperCase()).append(name.substring(1)).toString();
	}

	public static String getModifierNameForField(String name) {
		return new StringBuffer("set").append(name.substring(0, 1).toUpperCase()).append(name.substring(1)).toString();
	}

	public static String getNotNullValue(String value) {
		return value != null ? value : "";
	}

	public static String join(char sep, String... strings) {
		StringBuilder builder = new StringBuilder();
		for (String s : strings) {
			builder.append(s).append(sep);
		}
		return builder.deleteCharAt(builder.length() - 1).toString();
	}

	public static String join(char sep, List<String> strings) {
		StringBuilder builder = new StringBuilder();
		for (String s : strings) {
			builder.append(s).append(sep);
		}
		return builder.deleteCharAt(builder.length() - 1).toString();
	}

	public static String normalizeEmail(String email) {
		String emailDomain = "";
		if (email.length() > GMAIL_EMAIL_SUFFIX_LENGTH) {
			emailDomain = email.substring(email.length() - GMAIL_EMAIL_SUFFIX_LENGTH);
		}
		if (emailDomain.equalsIgnoreCase(GMAIL_EMAIL_SUFFIX)) {
			return email.substring(0, email.length() - GMAIL_EMAIL_SUFFIX_LENGTH).replaceAll("\\.", "") + emailDomain;
		} else {
			return email;
		}
	}

	public static Set<String> getEmailIdsAsList(String emails) {
		Set<String> emailList = null;
		if (StringUtil.isNotEmpty(emails)) {
			emailList = new HashSet<>();
			String[] emailIds = emails.split("[;:, \t\n\r\f]");
			for (String element : emailIds) {
				if (element.contains("@")) {
					element = element.replaceAll("[<>,\"]", "");
					emailList.add(element);
				}
			}
		}
		return emailList;
	}

	public static String getEmailDomain(String email) {
		return email.substring(email.lastIndexOf('@') + 1);
	}

	/** get amit.dalal from amit.dalal@xyz.com */
	public static String getLocalPartFromEmail(String emailAdd) {
		return emailAdd.substring(0, emailAdd.lastIndexOf('@'));
	}

	public static List<String> split(String input, String regex) {
		return Arrays.asList(input.split(regex));
	}

	public static List<String> split(String input) {
		return split(input, ",");
	}

	public static String capitalizeString(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = false;
		for (int i = 0; i < chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
				// You can add other chars here
				found = false;
			}
		}
		return String.valueOf(chars);
	}

	public static void main(String[] args) {
		String aplhaNumberic = getRandom().toLowerCase().replaceAll("[^\\da-z]", "");
		System.out.println(aplhaNumberic.substring(aplhaNumberic.length() - 32));
		System.out.println(parsePrice("1,00.00 Rs").intValue());
	}

	/** For JSON-P interaction */
	public static String callbackFunction(String functionName, String value) {
		StringBuilder outBuilder = new StringBuilder();
		return outBuilder.append(functionName).append("(").append(value).append(")").toString();
	}

	public static Double parsePrice(String number) {
		number = number.replaceAll("[^\\.\\d,-]", "");
		DecimalFormat df = new DecimalFormat("#,##,###.##");
		Number n;
		try {
			n = df.parse(number);
		} catch (ParseException e) {
			return 0.0;
		}
		return n.doubleValue();
	}

	public static String trim(String value, int endIndex) {
		if (value.length() > endIndex) {
			value = value.substring(0, endIndex - 3).concat("...");
		}
		return value;
	}

	public static String join(List<String> list, String separator) {
		StringBuilder sb = new StringBuilder();
		for (String s : list) {
			sb.append(s).append(separator);
		}
		sb.deleteCharAt(sb.length() - separator.length());
		return sb.toString();
	}

	public static String randomNString(Integer count, Boolean isUpperCase) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < count; i++) {
			char ch = (char) (new Random().nextInt(26) + 'a');
			stringBuilder.append(ch);
		}
		if (isUpperCase)
			return stringBuilder.toString().toUpperCase();

		return stringBuilder.toString();
	}

	public static boolean startsWithUppercase(String str) {
		if (str == null || "".equals(str.trim()))
			return false;
		else
			return Character.isUpperCase(str.charAt(0));
	}

	public static int getIncrementedValue(String obfuscatedValue) {
		String incrementedValue = StringUtil.isNotEmpty(obfuscatedValue)
				? obfuscatedValue.substring(10, obfuscatedValue.length() - 10)
				: "0";
		return Integer.parseInt(incrementedValue) / 7;
	}

	public static String getStacktrace(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

}
