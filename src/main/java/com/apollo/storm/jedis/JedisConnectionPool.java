package com.apollo.storm.jedis;

import org.apache.log4j.Logger;

import com.apollo.constants.Constant;
import com.apollo.util.ErrorHandler;
import com.apollo.util.StringUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class JedisConnectionPool {
	private static final Logger LOGGER = Logger.getLogger(JedisConnectionPool.class);

	private static JedisPool jedisPool;

	private JedisConnectionPool() {
	}

	public static synchronized Jedis getJedisConnection() {
		try {
			if (jedisPool == null) {
				JedisPoolConfig config = new JedisPoolConfig();
				config.setMaxTotal(1000);
				config.setMaxIdle(10);
				config.setMinIdle(1);
				config.setMaxWaitMillis(30000);
				if (StringUtil.isNotEmpty(Constant.REDIS_PASSWORD))
					jedisPool = new JedisPool(config, Constant.REDIS_HOST, Constant.REDIS_PORT, 0,
							Constant.REDIS_PASSWORD);
				else
					jedisPool = new JedisPool(config, Constant.REDIS_HOST, Constant.REDIS_PORT, 0);
			}
			return jedisPool.getResource();
		} catch (JedisConnectionException e) {
			LOGGER.error("Could not establish Redis connection.");
			ErrorHandler.sendErrorMail(e.getLocalizedMessage(),
					"Could not establish Redis connection. Is the Redis running?");
			throw e;
		}
	}

	public static synchronized void close(Jedis resource) {
		if (jedisPool != null) {
			try {
				if (resource != null) {
					resource.close();
				}
			} catch (JedisConnectionException e) {
				resource.close();
			}
		}
	}

	public static synchronized void destroyPool() {
		if (jedisPool != null)
			jedisPool.destroy();
	}

	public static JedisPool getJedisPool() {
		return jedisPool;
	}

	public static void setJedisPool(JedisPool jedisPool) {
		JedisConnectionPool.jedisPool = jedisPool;
	}
}
