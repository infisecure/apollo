package com.apollo.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DeviceInformationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private DeviceInformationDto() {
		super();
	}

	private String deviceCategory;
	private String osName;
	private String osFamily;
	private String osVersion;
	private String userAgent;
	private String userAgentType;
	private String userAgentFamily;
	private String userAgentVersion;

	private DeviceInformationDto(DeviceInformationBuilder deviceInformationBuilder) {
		super();
		this.deviceCategory = deviceInformationBuilder.deviceCategory;
		this.osName = deviceInformationBuilder.osName;
		this.osFamily = deviceInformationBuilder.osFamily;
		this.osVersion = deviceInformationBuilder.osVersion;
		this.userAgent = deviceInformationBuilder.userAgent;
		this.userAgentType = deviceInformationBuilder.userAgentType;
		this.userAgentFamily = deviceInformationBuilder.userAgentFamily;
		this.userAgentVersion = deviceInformationBuilder.userAgentVersion;
	}

	public static class DeviceInformationBuilder {
		private String deviceCategory;
		private String osName;
		private String osFamily;
		private String osVersion;
		private String userAgent;
		private String userAgentType;
		private String userAgentFamily;
		private String userAgentVersion;

		public DeviceInformationBuilder(String userAgent, String userAgentType, String userAgentFamily,
				String userAgentVersion) {
			this.userAgent = userAgent;
			this.userAgentType = userAgentType;
			this.userAgentFamily = userAgentFamily;
			this.userAgentVersion = userAgentVersion;
		}

		public DeviceInformationBuilder osDetails(String osName, String osFamily, String osVersion) {
			this.osName = osName;
			this.osFamily = osFamily;
			this.osVersion = osVersion;
			return this;
		}

		public DeviceInformationBuilder deviceCategory(String deviceCategory) {
			this.osName = deviceCategory;
			return this;
		}

		public DeviceInformationDto build() {
			return new DeviceInformationDto(this);
		}
	}
}
