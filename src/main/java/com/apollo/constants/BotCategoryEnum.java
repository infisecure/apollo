package com.apollo.constants;

public enum BotCategoryEnum {

	FILTERED_REQUEST("Filtered Request"), KNOWN_VIOLATOR("Known Violator"), BROWSER_INTEGRITY("Browser Integrity"),
	RATE_LIMITING("Rate Limiting"), INFISECURE_INTEGRITY("InfiSecure Integrity"),
	IP_USAGE_INTEGRITY("IP Usage Integrity"), IMPERSONATOR("Impersonator"), EMPTY_IP("Empty IP Address"),
	RESERVED_IP("Reserved IP Address");

	private String botCategory;

	private BotCategoryEnum(String botCategory) {
		this.botCategory = botCategory;
	}

	public String getBotCategory() {
		return botCategory;
	}

	@Override
	public String toString() {
		return botCategory;
	}

	public String getName() {
		return this.name();
	}

}
