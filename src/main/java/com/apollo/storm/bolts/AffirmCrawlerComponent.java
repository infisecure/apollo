package com.apollo.storm.bolts;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.NamingException;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apollo.constants.Constant;
import com.apollo.constants.KafkaTopicEnum;
import com.apollo.constants.RequestCategoryEnum;
import com.apollo.dto.DeviceInformationDto;
import com.apollo.dto.GoodBotInformationDto;
import com.apollo.dto.GoodBotInformationDto.GoodBotInformationBuilder;
import com.apollo.dto.HumanElasticDataDto;
import com.apollo.dto.HumanElasticDataDto.HumanElasticDataBuilder;
import com.apollo.dto.IPApiRespDto;
import com.apollo.dto.ServerRequestDataDto;
import com.apollo.dto.UriDetailsDto;
import com.apollo.kafka.ApolloProducer;
import com.apollo.storm.jedis.JedisConnectionPool;
import com.apollo.util.CacheHandlerUtil;
import com.apollo.util.DateUtil;
import com.apollo.util.NSLookupService;
import com.apollo.util.StringUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;

public class AffirmCrawlerComponent extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AffirmCrawlerComponent.class);

	private ObjectMapper mapper;
	private transient OutputCollector outputCollector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map stormConfigMap, TopologyContext context,
			OutputCollector collector) {
		LOGGER.info("Preparing Affirm Crawler Component Task Bolt.");
		int totalTasks = context.getComponentTasks(RequestCategoryEnum.AFFIRM_CRAWLER.getRequestCategory()).size();
		LOGGER.info("Total Task, {}", totalTasks);
		this.outputCollector = collector;
		this.mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
	}

	@Override
	public void execute(final Tuple tuple) {
		Jedis jedis = null;

		/** get input data from the tuple */
		String inputData = tuple.getString(0);
		if (StringUtil.isNotEmpty(inputData)) {
			LOGGER.info("Processing Affirm Crawler API data, {}", inputData);
			ServerRequestDataDto requestDataDto = null;
			try {
				/** convert json string to request object */
				requestDataDto = mapper.readValue(inputData, ServerRequestDataDto.class);

				/** get domain code from the request */
				String domainCode = requestDataDto.getIsar1();
				String ipAddress = StringUtil.getNotNullValue(requestDataDto.getIsar9()).trim();

				/** get path and page name from the requested url */
				String requestedUrl = StringUtil.getNotNullValue(requestDataDto.getIsar4()).trim();
				UriDetailsDto uriDetailsDto = CacheHandlerUtil.processUrl(requestedUrl);

				/** get device details details from the user-agent */
				String userAgent = requestDataDto.getIsar10();
				DeviceInformationDto deviceInfoDto = CacheHandlerUtil.getUseragentDetails(userAgent);

				/** check if the crawler key is already present */
				jedis = JedisConnectionPool.getJedisConnection();

				String crawlerCode = jedis.hget(Constant.CRAWLER_UA_CODE_MAP_KEY, userAgent);
				String crawlerDetailsKey = new StringBuilder(Constant.CRAWLER_DETAILS_HASH_KEY)
						.append(Constant.COLON_DELIMITER).append(crawlerCode).toString();
				Map<String, String> crawlerDetailsMap = jedis.hgetAll(crawlerDetailsKey);

				String crawlerFamily = StringUtil.getNotNullValue(crawlerDetailsMap.get(Constant.CRAWLER_FAMILY_KEY));
				String validationChecks = jedis.hget(Constant.CRAWLER_VALIDATION_CHECKS_HASH_KEY, crawlerFamily);
				if (executeVaidationChecks(ipAddress, validationChecks)) {
					String crawlerName = StringUtil.getNotNullValue(crawlerDetailsMap.get(Constant.CRAWLER_NAME_KEY));
					String crawlerCategory = StringUtil.getNotNullValue(Constant.CRAWLER_CATEGORY_KEY);
					GoodBotInformationDto crawlerAttributesDto = new GoodBotInformationBuilder(domainCode, ipAddress)
							.crawlerFamily(crawlerFamily).crawlerName(crawlerName).crawlerCategory(crawlerCategory)
							.build();
					ApolloProducer.publishMessage(KafkaTopicEnum.CREATE_CRAWLER.getKafkaTopic(),
							mapper.writeValueAsString(crawlerAttributesDto));
				}

				/**
				 * convert the time stamp to date and get minute, hour and day of week
				 */
				long requestTime = Long.parseLong(requestDataDto.getIsar8());
				Date apiHitTime = new Date(requestTime);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(apiHitTime);
				int minute = calendar.get(Calendar.MINUTE);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

				String pageSequenceCookie = StringUtil.getNotNullValue(requestDataDto.getIsar14().trim());
				String[] pageSequenceCookieArr = pageSequenceCookie.split("\\.");
				int pageNumber = StringUtil.isNotEmpty(pageSequenceCookieArr[2])
						? Integer.parseInt(pageSequenceCookieArr[2])
						: 0;

				Date firstSeen = null;
				int daysDifference = 0;
				long firstSeenTimestamp = 0;
				String[] identifierCookie = requestDataDto.getIsar12().trim().split("\\.");
				if (identifierCookie.length == 3) {
					firstSeenTimestamp = Long.parseLong(identifierCookie[identifierCookie.length - 1]);
					firstSeen = new Date(firstSeenTimestamp);
					daysDifference = DateUtil.getDaysDiff(apiHitTime, firstSeen);
				}

				long sessionDuration = DateUtil.getSessionDuration(requestTime, pageSequenceCookieArr[1]);
				long processingTime = Long.parseLong(requestDataDto.getIsar35()) - requestTime;

				HumanElasticDataDto humanElasticDataDto = new HumanElasticDataBuilder(requestDataDto)
						.pageDetails(uriDetailsDto).deviceDetails(deviceInfoDto).minute(minute).hour(hour)
						.dayOfWeek(dayOfWeek).numDaysActive(daysDifference).sessionDuration(sessionDuration)
						.pageSequence(pageNumber).processingTimeDifference(processingTime).build();
				ApolloProducer.publishMessage(KafkaTopicEnum.HUMAN.getKafkaTopic(),
						mapper.writeValueAsString(humanElasticDataDto));

				outputCollector.ack(tuple);
			} catch (IOException e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception while parsing input data, {}", e);
			} catch (Exception e) {
				outputCollector.fail(tuple);
				LOGGER.error("Exception, {}", e);
			} finally {
				JedisConnectionPool.close(jedis);
			}
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer arg0) {
		// End of operation, the data is enriched and written to Kafka queue in
		// this bolt
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public OutputCollector getOutputCollector() {
		return outputCollector;
	}

	public void setOutputCollector(OutputCollector outputCollector) {
		this.outputCollector = outputCollector;
	}

	private boolean executeVaidationChecks(final String ipAddress, final String validationChecks) {
		boolean isCrawler = false;
		List<String> validationChecksList = Arrays.asList(validationChecks.split("\\s*,\\s*"));
		for (final String validationCheck : validationChecksList) {
			if (("DNS".equals(validationCheck) && checkIsDNSCrawler(ipAddress))
					|| ("ISP".equals(validationCheck) && checkIsISPCrawler(ipAddress))
					|| ("USERAGENT".equals(validationCheck))) {
				isCrawler = true;
				break;
			}
		}
		return isCrawler;
	}

	private boolean checkIsDNSCrawler(String ipAddress) {
		boolean isGoodDNS = false;
		Jedis jedis = null;
		try {
			String dnsname = NSLookupService.doNSLookup(ipAddress);
			if (dnsname == null)
				return isGoodDNS;

			jedis = JedisConnectionPool.getJedisConnection();
			Map<String, String> goodDnsSet = jedis.hgetAll(Constant.CRAWLER_DOMAIN_HASH_KEY);
			for (Entry<String, String> goodDns : goodDnsSet.entrySet()) {
				if (dnsname.endsWith((String) goodDns.getKey())) {
					isGoodDNS = true;
					break;
				}
			}
		} catch (IOException | NamingException e) {
			LOGGER.error("Exception while DNS validation, {}", e);
		} finally {
			JedisConnectionPool.close(jedis);
		}
		return isGoodDNS;
	}

	private boolean checkIsISPCrawler(String ipAddress) {
		boolean isGoodISP = false;
		Jedis jedis = null;
		try {
			IPApiRespDto ipApiRespDto = CacheHandlerUtil.getIpDetails(ipAddress);
			String isp = StringUtil.getNotNullValue(ipApiRespDto.getIsp());
			/* ignoring gcloud machine */
			if (StringUtil.isEmpty(ipApiRespDto.getIsp()) || isp.toLowerCase().contains("googleusercontent"))
				return isGoodISP;

			jedis = JedisConnectionPool.getJedisConnection();
			Map<String, String> ip2LocIspMap = jedis.hgetAll(Constant.CRAWLER_ISP_HASH_KEY);
			for (Entry<String, String> ip2LocsIsp : ip2LocIspMap.entrySet()) {
				if (isp.toLowerCase().contains(ip2LocsIsp.getKey().toLowerCase())) {
					isGoodISP = true;
					break;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception while ISP validation, {}", e);
		} finally {
			JedisConnectionPool.close(jedis);
		}
		return isGoodISP;
	}
}
