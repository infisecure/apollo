package com.apollo.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import com.apollo.dto.UriDetailsDto;

public class URIParserUtil {

	private URIParserUtil() {
	}

	private static final Logger LOGGER = Logger.getLogger(URIParserUtil.class);

	public static UriDetailsDto processUrl(String url) {

		String path = "";
		String filename = "";
		try {
			if (StringUtil.isEmpty(url)) {
				return new UriDetailsDto("", "");
			}

			if ("/".equals(url)) {
				return new UriDetailsDto("/", "/");
			}

			filename = StringUtil.isNotEmpty(Paths.get(new URI(url).getPath()).getFileName().toString())
					? Paths.get(new URI(url).getPath()).getFileName().toString()
					: "";
			path = StringUtil.isNotEmpty(Paths.get(new URI(url).getPath()).getParent().toString())
					? Paths.get(new URI(url).getPath()).getParent().toString()
					: "";

		} catch (URISyntaxException e) {
			LOGGER.error("Exception parsing URI, " + e);
		}
		return new UriDetailsDto(path, filename);
	}

}
