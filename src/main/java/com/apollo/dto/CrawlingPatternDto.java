package com.apollo.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class CrawlingPatternDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String iscp1;
	private String iscp2;
	private String iscp3;
	private String iscp4;
	private String iscp5;
	private String iscp6;
	private String iscp7;
	private String iscp8;
	private Date iscp9;
	private int iscp10;
	private Date iscp11;
	private int iscp12;
	private String iscp13;
	private String iscp14;

}
